#include "audioprobe.h"
#include <QAudioProbe>
#include <QDebug>
#include <QMediaObject>
#include <QMediaPlayer>
#include <QAudioDecoder>
#include <QAudioDeviceInfo>
#include <QAudioInput>
#include <QAudioRecorder>

AudioProbe::AudioProbe(QObject *parent)
    : QObject(parent)
{
    calculator = new FFTCalc(this);
    m_player = new QMediaPlayer;
    connect(calculator, &FFTCalc::calculatedSpectrum, this, [this](QVector<double> spectrum) {
        int size = 20;
        m_spectrum.resize(size);
        int j = 0;
        for (int i = 0; i < spectrum.size(); i += spectrum.size()/(size - 1)) {
            m_spectrum[j] = spectrum[i];
            ++j;
        }
        emit spectrumChanged();
    });
}

void AudioProbe::setupSource() {
    QAudioProbe *probe = new QAudioProbe;
    probe->setSource(m_player);
    if(probe->setSource(m_player))
        qDebug() << probe->isActive();
    connect(probe, SIGNAL(audioBufferProbed(QAudioBuffer)), this, SLOT(processBuffer(QAudioBuffer)));
    return;
}

void AudioProbe::processBuffer(QAudioBuffer buffer){
    qreal peakValue;
    int duration;

    if(buffer.frameCount() < 512)
        return;

    levelLeft = levelRight = 0;

    if(buffer.format().channelCount() != 2)
        return;

    sample.resize(buffer.frameCount());
    if(buffer.format().sampleType() == QAudioFormat::SignedInt){
        QAudioBuffer::S16S *data = buffer.data<QAudioBuffer::S16S>();
        if (buffer.format().sampleSize() == 32)
            peakValue=INT_MAX;
        else if (buffer.format().sampleSize() == 16)
            peakValue=SHRT_MAX;
        else
            peakValue=CHAR_MAX;

        for(int i=0; i<buffer.frameCount(); i++){
            sample[i] = data[i].left/peakValue;
            levelLeft+= abs(data[i].left)/peakValue;
            levelRight+= abs(data[i].right)/peakValue;
        }
    }

    else if(buffer.format().sampleType() == QAudioFormat::UnSignedInt){
        QAudioBuffer::S16U *data = buffer.data<QAudioBuffer::S16U>();
        if (buffer.format().sampleSize() == 32)
            peakValue=UINT_MAX;
        else if (buffer.format().sampleSize() == 16)
            peakValue=USHRT_MAX;
        else
            peakValue=UCHAR_MAX;
        for(int i=0; i<buffer.frameCount(); i++){
            sample[i] = data[i].left/peakValue;
            levelLeft+= abs(data[i].left)/peakValue;
            levelRight+= abs(data[i].right)/peakValue;
        }
    }

    else if(buffer.format().sampleType() == QAudioFormat::Float){
        QAudioBuffer::S32F *data = buffer.data<QAudioBuffer::S32F>();
        peakValue = 1.00003;
        for(int i=0; i<buffer.frameCount(); i++){
            sample[i] = data[i].left/peakValue;
            if(sample[i] != sample[i]){
                sample[i] = 0;
            }
            else{
                levelLeft+= abs(data[i].left)/peakValue;
                levelRight+= abs(data[i].right)/peakValue;
            }
        }
    }
    duration = buffer.format().durationForBytes(buffer.frameCount())/1000;
    calculator->calc(sample, duration);
    emit levels(levelLeft/buffer.frameCount(), levelRight/buffer.frameCount());
}

void AudioProbe::playURL(const QString &filename) {
    m_player->setMedia(QUrl(filename));
    m_player->play();
    setPlaybackState(QMediaPlayer::PlayingState);
    connect(m_player, &QMediaPlayer::durationChanged, this, [&](qint64 dur) {
        emit durationChanged(dur);
    });
    connect(m_player, &QMediaPlayer::positionChanged, this, [&](qint64 pos) {
        emit positionChanged(pos);
    });
}

void AudioProbe::playerStop(){
    m_player->stop();
    setPlaybackState(QMediaPlayer::StoppedState);
}

void AudioProbe::playerPause(){
    m_player->pause();
    setPlaybackState(QMediaPlayer::PausedState);
}

void AudioProbe::playerContinue(){
    m_player->play();
    setPlaybackState(QMediaPlayer::PlayingState);
}

void AudioProbe::setPlaybackState(QMediaPlayer::State playbackState){
    m_playerState = playbackState;
    emit playbackStateChanged(playbackState);
}

void AudioProbe::playerSeek(qint64 seekvalue){
    m_player->setPosition(seekvalue);
}

QMediaPlayer::State AudioProbe::playbackState() const{
    return m_player->state();
}
