/*
 *   Copyright (C) 2019 by Aditya Mehra <aix.m@outlook.com>                      *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "automobiledashboard_dbus.h"
#include "automobiledashboardplugin.h"
#include <QMetaObject>
#include <QByteArray>
#include <QList>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QVariant>
#include <QtDBus>
#include <QDebug>

/*
 * Implementation of adaptor class AutoDashDbusAdapterInterface
 */

AutoDashDbusAdapterInterface::AutoDashDbusAdapterInterface(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    // constructor
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.registerObject("/autodashapplet", this, QDBusConnection::ExportScriptableSlots | QDBusConnection::ExportNonScriptableSlots);
    dbus.registerService("org.kde.autodashapplet");
    setAutoRelaySignals(true);
}

AutoDashDbusAdapterInterface::~AutoDashDbusAdapterInterface()
{
    // destructor
}

void AutoDashDbusAdapterInterface::viewAutoDash()
{
    // handle method call org.kde.autodash.viewAutoDash
    emit sendShowAutoDash("ViewDashboard");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ViewDashboard"));
}

void AutoDashDbusAdapterInterface::viewNavigation()
{
    // handle method call org.kde.autodash.viewNavigtion
    emit sendShowNavigation("ViewNavigation");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ViewNavigation"));
}

void AutoDashDbusAdapterInterface::viewMusic()
{
    // handle method call org.kde.autodash.viewMusic
    emit sendShowMusic("ViewMusic");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ViewMusic"));
}

void AutoDashDbusAdapterInterface::viewMessages()
{
    // handle method call org.kde.autodash.viewMessages
    emit sendShowMessages("ViewMessages");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ViewMessages"));
}

void AutoDashDbusAdapterInterface::activateVoiceCmd()
{
    // handle method call org.kde.autodash.activateVoiceCmd
    emit sendActivateVoiceCmd("ActivateVoiceCmd");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ActivateVoiceCmd"));
}

void AutoDashDbusAdapterInterface::scSearchResult(const QString &scResult)
{
    emit sendScResult(scResult);
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, scResult));
}

void AutoDashDbusAdapterInterface::lcSearchResult(const QString &lcResult)
{
    emit sendLcResult(lcResult);
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, lcResult));
}

void AutoDashDbusAdapterInterface::navDestinationQuery(const QString &navDestination)
{
    emit sendNavDestinationQuery(navDestination);
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, navDestination));
}

void AutoDashDbusAdapterInterface::navShowNearbyPlaces()
{
    emit sendNavShowNearbyPlaces("ShowNearbyPlacesMenu");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ShowNearbyPlacesMenu"));
}

void AutoDashDbusAdapterInterface::navNearbyPlaceQuery(const QString &navNearbyPlace)
{
    emit sendNearbyPlacesQuery(navNearbyPlace);
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, navNearbyPlace));
}

void AutoDashDbusAdapterInterface::navShowDirections()
{
    emit sendNavShowDirections("ShowNavDirections");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "ShowNavDirections"));
}

void AutoDashDbusAdapterInterface::internalDashLockHood()
{
    emit sigInternalDashLockHood("InternalDashLockHood");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashLockHood"));
}

void AutoDashDbusAdapterInterface::internalDashUnLockHood()
{
    emit sigInternalDashUnLockHood("InternalDashUnLockHood");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashUnLockHood"));
}

void AutoDashDbusAdapterInterface::internalDashLockTrunk()
{
    emit sigInternalDashLockTrunk("InternalDashLockTrunk");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashLockTrunk"));
}

void AutoDashDbusAdapterInterface::internalDashUnLockTrunk()
{
    emit sigInternalDashUnLockTrunk("InternalDashUnLockTrunk");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashUnLockTrunk"));
}

void AutoDashDbusAdapterInterface::internalDashLockLeftDoor()
{
    emit sigInternalDashLockLeftDoor("InternalDashLockLeftDoor");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashLockLeftDoor"));
}

void AutoDashDbusAdapterInterface::internalDashUnLockLeftDoor()
{
    emit sigInternalDashUnLockLeftDoor("InternalDashUnLockLeftDoor");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashUnLockLeftDoor"));
}

void AutoDashDbusAdapterInterface::internalDashLockRightDoor()
{
    emit sigInternalDashLockRightDoor("InternalDashLockRightDoor");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashLockRightDoor"));
}

void AutoDashDbusAdapterInterface::internalDashUnLockRightDoor()
{
    emit sigInternalDashUnLockRightDoor("InternalDashUnLockRightDoor");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashUnLockRightDoor"));
}

void AutoDashDbusAdapterInterface::internalDashHeadlightOn()
{
    emit sigInternalDashHeadlightOn("InternalDashHeadlightOn");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashHeadlightOn"));
}

void AutoDashDbusAdapterInterface::internalDashHeadlightOff()
{
    emit sigInternalDashHeadlightOff("InternalDashHeadlightOff");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashHeadlightOff"));
}

void AutoDashDbusAdapterInterface::internalDashHeadlightLevel(int iDashHeadLightLvl)
{
    emit sigInternalDashHeadlightLevel(iDashHeadLightLvl);
}

void AutoDashDbusAdapterInterface::internalDashWiperBladeOn()
{
    emit sigInternalDashWiperBladeOn("InternalDashWiperBladeOn");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashWiperBladeOn"));
}

void AutoDashDbusAdapterInterface::internalDashWiperBladeOff()
{
    emit sigInternalDashWiperBladeOff("InternalDashWiperBladeOff");
    QMetaObject::invokeMethod(this, "getMethod", Qt::DirectConnection, Q_ARG(QString, "InternalDashWiperBladeOff"));
}

void AutoDashDbusAdapterInterface::internalDashWiperBladeLevel(int iDashWiperBladeLvl)
{
    emit sigInternalDashWiperBladeLevel(iDashWiperBladeLvl);
}

void AutoDashDbusAdapterInterface::internalDashAdjAcTemp(const QString &acTempAdjustmentType, int acTempAdjustmentLevel)
{
    emit sigInternalDashAdjAcTemp(acTempAdjustmentType, acTempAdjustmentLevel);
}

void AutoDashDbusAdapterInterface::internalDashAdjLeftSeat(const QString &leftSeatAdjustmentType, int leftSeatAdjustmentLevel)
{
    emit sigInternalDashAdjLeftSeat(leftSeatAdjustmentType, leftSeatAdjustmentLevel);
}

void AutoDashDbusAdapterInterface::internalDashAdjRightSeat(const QString &rightSeatAdjustmentType, int rightSeatAdjustmentLevel)
{
    emit sigInternalDashAdjRightSeat(rightSeatAdjustmentType, rightSeatAdjustmentLevel);
}

void AutoDashDbusAdapterInterface::internalDashAdjSteerHeight(const QString &steerHeightAdjustmentType, int steerHeightAdjustmentLevel)
{
    emit sigInternalDashAdjSteerHeight(steerHeightAdjustmentType, steerHeightAdjustmentLevel);
}

void AutoDashDbusAdapterInterface::internalDashLights(bool iDashLightControl)
{
    emit sigInternalDashLights(iDashLightControl);
}

void AutoDashDbusAdapterInterface::navigationChangeMapMode(const QString &navChangeMapMode)
{
    emit sigNavigationChangeMapMode(navChangeMapMode);
}

void AutoDashDbusAdapterInterface::navigationChangeMapStyle(const QString &navChangeMapStyle)
{
    emit sigNavigationChangeMapStyle(navChangeMapStyle);
}

void AutoDashDbusAdapterInterface::navigationFindNearbyPlacesType(const QString &navFindNearbyPlacesType)
{
    emit sigNavigationFindNearbyPlacesType(navFindNearbyPlacesType);
}

void AutoDashDbusAdapterInterface::navigationGetToLocation(const QString &navToLocation)
{
    emit sigNavigationGetToLocation(navToLocation);
}

void AutoDashDbusAdapterInterface::navigationShowHomeLocation()
{
    emit sigNavigationShowHomeLocation("ShowHome");
}

void AutoDashDbusAdapterInterface::radioPlayerChangeMode(const QString &radioPlayerMode)
{
    emit sigRadioPlayerChangeMode(radioPlayerMode);
}

void AutoDashDbusAdapterInterface::radioPlayerScanStations(const QString &radioPlayerScan)
{
    emit sigRadioPlayerScanStations(radioPlayerScan);
}

void AutoDashDbusAdapterInterface::radioPlayerSelectStation(int radioPlayerStation)
{
    emit sigRadioPlayerSelectStation(radioPlayerStation);
}

void AutoDashDbusAdapterInterface::radioPlayerTurnOff()
{
    emit sigRadioPlayerTurnOff("Off");
}

void AutoDashDbusAdapterInterface::radioPlayerTurnOn()
{
    emit sigRadioPlayerTurnOn("On");
}

void AutoDashDbusAdapterInterface::musicPlayerChangeMode(const QString &musicPlayerMode)
{
    emit sigMusicPlayerChangeMode(musicPlayerMode);
}

void AutoDashDbusAdapterInterface::musicPlayerNextTrack()
{
    emit sigMusicPlayerNextTrack("Next");
}

void AutoDashDbusAdapterInterface::musicPlayerPauseTrack()
{
    emit sigMusicPlayerPauseTrack("Pause");
}

void AutoDashDbusAdapterInterface::musicPlayerPlaySongFromList(const QString &musicPlayerPlayUrl, const QString &musicPlayerPlayThumb, const QString &musicPlayerPlayTitle)
{
    emit sigMusicPlayerPlaySongFromList(musicPlayerPlayUrl, musicPlayerPlayThumb, musicPlayerPlayTitle);
}

void AutoDashDbusAdapterInterface::musicPlayerPreviousTrack()
{
    emit sigMusicPlayerPreviousTrack("Previous");
}

void AutoDashDbusAdapterInterface::musicPlayerStopTrack()
{
    emit sigMusicPlayerStopTrack("Stop");
}

void AutoDashDbusAdapterInterface::showNotif(const QString &popShowNotif)
{
    emit sigShowNotif(popShowNotif);
}

Q_INVOKABLE QString AutoDashDbusAdapterInterface::getMethod(const QString &method)
{
    QString str = method;
    return str;
}

