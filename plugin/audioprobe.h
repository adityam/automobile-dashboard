#ifndef AUDIOPROBE_H
#define AUDIOPROBE_H

#include <QObject>
#include <QAudioProbe>
#include <QVector>
#include <QMediaPlayer>
#include "fftcalc.h"


class AudioProbe : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVector<double> spectrum READ spectrum NOTIFY spectrumChanged)
    Q_PROPERTY(QMediaPlayer::State playbackState READ playbackState NOTIFY playbackStateChanged)

public:
    explicit AudioProbe(QObject *parent = Q_NULLPTR);

    QMediaPlayer::State playerState() const {return m_playerState;}
    QVector<double> spectrum() const {return m_spectrum;}
    
public Q_SLOTS:
    void setupSource();
    void processBuffer(QAudioBuffer buffer);
    void playURL(const QString &filename);
    void playerStop();
    void playerPause();
    void playerContinue();
    void playerSeek(qint64 seekvalue);
    QMediaPlayer::State playbackState() const;
    void setPlaybackState(QMediaPlayer::State playbackState);
    
Q_SIGNALS:
    void playbackStateChanged(QMediaPlayer::State pbstate);
    void durationChanged(qint64 dur);
    void positionChanged(qint64 pos);

private:
    QVector<double> sample;
    QVector<double> m_spectrum;
    QMediaPlayer::State m_playerState;
    double levelLeft, levelRight;
    FFTCalc *calculator;
    QMediaPlayer *m_player;

signals:
    int levels(double left, double right);
    void spectrumChanged();
};

#endif // AUDIOPROBE_H
