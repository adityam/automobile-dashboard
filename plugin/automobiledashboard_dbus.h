/*
 *   Copyright (C) 2019 by Aditya Mehra <aix.m@outlook.com>                      *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef AUTODASHPLASMOID_DBUS_H
#define AUTODASHPLASMOID_DBUS_H

#include <QObject>
#include <QtDBus>
QT_BEGIN_NAMESPACE
class QByteArray;
template<class T> class QList;
template<class Key, class Value> class QMap;
class QString;
class QStringList;
class QVariant;
QT_END_NAMESPACE

/*
 * Adaptor class for interface org.kde.autodashapplet
 */
class AutoDashDbusAdapterInterface: public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.kde.autodashapplet")
    Q_CLASSINFO("D-Bus Introspection", ""
"  <interface name=\"org.kde.autodashapplet\">\n"
"    <signal name=\"sigShowNotif\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgShowNotif\"/>\n"
"    </signal>\n"
"    <signal name=\"sendShowAutoDash\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgShowAutoDash\"/>\n"
"    </signal>\n"
"    <signal name=\"sendShowNavigation\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgShowNavigation\"/>\n"
"    </signal>\n"
"    <signal name=\"sendShowMusic\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgShowMusic\"/>\n"
"    </signal>\n"
"    <signal name=\"sendShowMessages\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgShowMessages\"/>\n"
"    </signal>\n"
"    <signal name=\"sendActivateVoiceCmd\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgShowActivateVoiceCmd\"/>\n"
"    </signal>\n"
"    <signal name=\"sendScResult\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgScResult\"/>\n"
"    </signal>\n"
"    <signal name=\"sendLcResult\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgLcResult\"/>\n"
"    </signal>\n"
"    <signal name=\"sendNavDestinationQuery\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavDestinationResult\"/>\n"
"    </signal>\n"
"    <signal name=\"sendNavShowNearbyPlaces\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavShowNearbyPlaces\"/>\n"
"    </signal>\n"
"    <signal name=\"sendNearbyPlacesQuery\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNearbyPlace\"/>\n"
"    </signal>\n"
"    <signal name=\"sendNavShowDirections\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavShowDirections\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashLockHood\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashLockHood\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashUnLockHood\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashUnLockHood\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashLockTrunk\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashLockTrunk\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashUnLockTrunk\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashUnLockTrunk\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashLockLeftDoor\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashLockLeftDoor\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashUnLockLeftDoor\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashUnLockLeftDoor\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashLockRightDoor\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashLockRightDoor\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashUnLockRightDoor\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashUnLockRightDoor\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashHeadlightOn\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashHeadlightOn\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashHeadlightOff\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashHeadlightOff\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashHeadlightLevel\">\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgInternalDashHeadlightLevel\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashWiperBladeOn\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashWiperBladeOn\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashWiperBladeOff\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashWiperBladeOff\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashWiperBladeLevel\">\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgInternalDashWiperBladeLevel\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashAdjLeftSeat\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashAdjLeftSeatType\"/>\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgInternalDashAdjLeftSeat\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashAdjRightSeat\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashAdjRightSeatType\"/>\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgInternalDashAdjRightSeat\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashAdjAcTemp\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashAdjAcTempType\"/>\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgInternalDashAdjAcTemp\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashAdjSteerHeight\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgInternalDashAdjSteerHeightType\"/>\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgInternalDashAdjSteerHeight\"/>\n"
"    </signal>\n"
"    <signal name=\"sigInternalDashLights\">\n"
"      <arg direction=\"out\" type=\"b\" name=\"msgInternalDashLights\"/>\n"
"    </signal>\n"
"    <signal name=\"sigNavigationGetToLocation\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavigationGetToLocation\"/>\n"
"    </signal>\n"
"    <signal name=\"sigNavigationShowHomeLocation\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavigationShowHomeLocation\"/>\n"
"    </signal>\n"
"    <signal name=\"sigNavigationChangeMapMode\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavigationChangeMapMode\"/>\n"
"    </signal>\n"
"    <signal name=\"sigNavigationChangeMapStyle\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavigationChangeMapStyle\"/>\n"
"    </signal>\n"
"    <signal name=\"sigNavigationFindNearbyPlacesType\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgNavigationFindNearbyPlacesType\"/>\n"
"    </signal>\n"
"    <signal name=\"sigRadioPlayerTurnOn\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgRadioPlayerTurnOn\"/>\n"
"    </signal>\n"
"    <signal name=\"sigRadioPlayerTurnOff\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgRadioPlayerTurnOff\"/>\n"
"    </signal>\n"
"    <signal name=\"sigRadioPlayerChangeMode\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgRadioPlayerChangeMode\"/>\n"
"    </signal>\n"
"    <signal name=\"sigRadioPlayerScanStations\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgRadioPlayerScanStations\"/>\n"
"    </signal>\n"
"    <signal name=\"sigRadioPlayerSelectStation\">\n"
"      <arg direction=\"out\" type=\"i\" name=\"msgRadioPlayerSelectStation\"/>\n"
"    </signal>\n"
"    <signal name=\"sigMusicPlayerChangeMode\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerChangeMode\"/>\n"
"    </signal>\n"
"    <signal name=\"sigMusicPlayerPlaySongFromList\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerPlayUrl\"/>\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerPlayThumb\"/>\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerPlayTitle\"/>\n"
"    </signal>\n"
"    <signal name=\"sigMusicPlayerNextTrack\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerNextTrack\"/>\n"
"    </signal>\n"
"    <signal name=\"sigMusicPlayerPreviousTrack\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerPreviousTrack\"/>\n"
"    </signal>\n"
"    <signal name=\"sigMusicPlayerPauseTrack\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerPauseTrack\"/>\n"
"    </signal>\n"
"    <signal name=\"sigMusicPlayerStopTrack\">\n"
"      <arg direction=\"out\" type=\"s\" name=\"msgMusicPlayerStopTrack\"/>\n"
"    </signal>\n"
"    <method name=\"showNotif\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"popShowNotif\"/>\n"
"    </method>\n"
"    <method name=\"radioPlayerTurnOn\"/>\n"
"    <method name=\"radioPlayerTurnOff\"/>\n"
"    <method name=\"radioPlayerChangeMode\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"radioPlayerMode\"/>\n"
"    </method>\n"
"    <method name=\"radioPlayerScanStations\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"radioPlayerScan\"/>\n"
"    </method>\n"
"    <method name=\"radioPlayerSelectStation\">\n"
"      <arg direction=\"in\" type=\"i\" name=\"radioPlayerStation\"/>\n"
"    </method>\n"
"    <method name=\"musicPlayerChangeMode\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"musicPlayerMode\"/>\n"
"    </method>\n"
"    <method name=\"musicPlayerPlaySongFromList\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"musicPlayerPlayUrl\"/>\n"
"      <arg direction=\"in\" type=\"s\" name=\"musicPlayerPlayThumb\"/>\n"
"      <arg direction=\"in\" type=\"s\" name=\"musicPlayerPlayTitle\"/>\n"
"    </method>\n"
"    <method name=\"musicPlayerNextTrack\"/>\n"
"    <method name=\"musicPlayerPreviousTrack\"/>\n"
"    <method name=\"musicPlayerPauseTrack\"/>\n"
"    <method name=\"musicPlayerStopTrack\"/>\n"
"    <method name=\"navigationGetToLocation\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"navToLocation\"/>\n"
"    </method>\n"
"    <method name=\"navigationShowHomeLocation\"/>\n"
"    <method name=\"navigationChangeMapMode\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"navChangeMapMode\"/>\n"
"    </method>\n"
"    <method name=\"navigationChangeMapStyle\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"navChangeMapStyle\"/>\n"
"    </method>\n"
"    <method name=\"navigationFindNearbyPlacesType\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"navFindNearbyPlacesType\"/>\n"
"    </method>\n"
"    <method name=\"viewAutoDash\"/>\n"
"    <method name=\"viewNavigation\"/>\n"
"    <method name=\"viewMusic\"/>\n"
"    <method name=\"viewMessages\"/>\n"
"    <method name=\"activateVoiceCmd\"/>\n"
"    <method name=\"scSearchResult\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"scResult\"/>\n"
"    </method>\n"
"    <method name=\"lcSearchResult\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"lcResult\"/>\n"
"    </method>\n"
"    <method name=\"navDestinationQuery\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"navDestination\"/>\n"
"    </method>\n"
"    <method name=\"navShowNearbyPlaces\"/>\n"
"    <method name=\"navNearbyPlaceQuery\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"navNearbyPlace\"/>\n"
"    </method>\n"
"    <method name=\"navShowDirections\"/>\n"
"    <method name=\"internalDashLockHood\"/>\n"
"    <method name=\"internalDashUnLockHood\"/>\n"
"    <method name=\"internalDashLockTrunk\"/>\n"
"    <method name=\"internalDashUnLockTrunk\"/>\n"
"    <method name=\"internalDashLockLeftDoor\"/>\n"
"    <method name=\"internalDashUnLockLeftDoor\"/>\n"
"    <method name=\"internalDashLockRightDoor\"/>\n"
"    <method name=\"internalDashUnLockRightDoor\"/>\n"
"    <method name=\"internalDashHeadlightOn\"/>\n"
"    <method name=\"internalDashHeadlightOff\"/>\n"
"    <method name=\"internalDashHeadlightLevel\">\n"
"      <arg direction=\"in\" type=\"i\" name=\"iDashHeadLightLvl\"/>\n"
"    </method>\n"
"    <method name=\"internalDashWiperBladeOn\"/>\n"
"    <method name=\"internalDashWiperBladeOff\"/>\n"
"    <method name=\"internalDashWiperBladeLevel\">\n"
"      <arg direction=\"in\" type=\"i\" name=\"iDashWiperBladeLvl\"/>\n"
"    </method>\n"
"    <method name=\"internalDashAdjLeftSeat\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"leftSeatAdjustmentType\"/>\n"
"      <arg direction=\"in\" type=\"i\" name=\"leftSeatAdjustmentLevel\"/>\n"
"    </method>\n"
"    <method name=\"internalDashAdjRightSeat\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"rightSeatAdjustmentType\"/>\n"
"      <arg direction=\"in\" type=\"i\" name=\"rightSeatAdjustmentLevel\"/>\n"
"    </method>\n"
"    <method name=\"internalDashAdjAcTemp\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"acTempAdjustmentType\"/>\n"
"      <arg direction=\"in\" type=\"i\" name=\"acTempAdjustmentLevel\"/>\n"
"    </method>\n"
"    <method name=\"internalDashAdjSteerHeight\">\n"
"      <arg direction=\"in\" type=\"s\" name=\"steerHeightAdjustmentType\"/>\n"
"      <arg direction=\"in\" type=\"i\" name=\"steerHeightAdjustmentLevel\"/>\n"
"    </method>\n"
"    <method name=\"internalDashLights\">\n"
"      <arg direction=\"in\" type=\"b\" name=\"iDashLightControl\"/>\n"
"    </method>\n"
"  </interface>\n"
        "")
public:
    AutoDashDbusAdapterInterface(QObject *parent);
    virtual ~AutoDashDbusAdapterInterface();
    Q_INVOKABLE QString getMethod(const QString &method);

public: // PROPERTIES
public Q_SLOTS: // METHODS
    void activateVoiceCmd();
    void internalDashAdjAcTemp(const QString &acTempAdjustmentType, int acTempAdjustmentLevel);
    void internalDashAdjLeftSeat(const QString &leftSeatAdjustmentType, int leftSeatAdjustmentLevel);
    void internalDashAdjRightSeat(const QString &rightSeatAdjustmentType, int rightSeatAdjustmentLevel);
    void internalDashAdjSteerHeight(const QString &steerHeightAdjustmentType, int steerHeightAdjustmentLevel);
    void internalDashHeadlightLevel(int iDashHeadLightLvl);
    void internalDashHeadlightOff();
    void internalDashHeadlightOn();
    void internalDashLights(bool iDashLightControl);
    void internalDashLockHood();
    void internalDashLockLeftDoor();
    void internalDashLockRightDoor();
    void internalDashLockTrunk();
    void internalDashUnLockHood();
    void internalDashUnLockLeftDoor();
    void internalDashUnLockRightDoor();
    void internalDashUnLockTrunk();
    void internalDashWiperBladeLevel(int iDashWiperBladeLvl);
    void internalDashWiperBladeOff();
    void internalDashWiperBladeOn();
    void lcSearchResult(const QString &lcResult);
    void musicPlayerChangeMode(const QString &musicPlayerMode);
    void musicPlayerNextTrack();
    void musicPlayerPauseTrack();
    void musicPlayerPlaySongFromList(const QString &musicPlayerPlayUrl, const QString &musicPlayerPlayThumb, const QString &musicPlayerPlayTitle);
    void musicPlayerPreviousTrack();
    void musicPlayerStopTrack();
    void navDestinationQuery(const QString &navDestination);
    void navNearbyPlaceQuery(const QString &navNearbyPlace);
    void navShowDirections();
    void navShowNearbyPlaces();
    void navigationChangeMapMode(const QString &navChangeMapMode);
    void navigationChangeMapStyle(const QString &navChangeMapStyle);
    void navigationFindNearbyPlacesType(const QString &navFindNearbyPlacesType);
    void navigationGetToLocation(const QString &navToLocation);
    void navigationShowHomeLocation();
    void radioPlayerChangeMode(const QString &radioPlayerMode);
    void radioPlayerScanStations(const QString &radioPlayerScan);
    void radioPlayerSelectStation(int radioPlayerStation);
    void radioPlayerTurnOff();
    void radioPlayerTurnOn();
    void scSearchResult(const QString &scResult);
    void showNotif(const QString &popShowNotif);
    void viewAutoDash();
    void viewMessages();
    void viewMusic();
    void viewNavigation();
    
Q_SIGNALS: // SIGNALS
    void sendActivateVoiceCmd(const QString &msgShowActivateVoiceCmd);
    void sendLcResult(const QString &msgLcResult);
    void sendNavDestinationQuery(const QString &msgNavDestinationResult);
    void sendNavShowDirections(const QString &msgNavShowDirections);
    void sendNavShowNearbyPlaces(const QString &msgNavShowNearbyPlaces);
    void sendNearbyPlacesQuery(const QString &msgNearbyPlace);
    void sendScResult(const QString &msgScResult);
    void sendShowAutoDash(const QString &msgShowAutoDash);
    void sendShowMessages(const QString &msgShowMessages);
    void sendShowMusic(const QString &msgShowMusic);
    void sendShowNavigation(const QString &msgShowNavigation);
    void sigInternalDashAdjAcTemp(const QString &msgInternalDashAdjAcTempType, int msgInternalDashAdjAcTemp);
    void sigInternalDashAdjLeftSeat(const QString &msgInternalDashAdjLeftSeatType, int msgInternalDashAdjLeftSeat);
    void sigInternalDashAdjRightSeat(const QString &msgInternalDashAdjRightSeatType, int msgInternalDashAdjRightSeat);
    void sigInternalDashAdjSteerHeight(const QString &msgInternalDashAdjSteerHeightType, int msgInternalDashAdjSteerHeight);
    void sigInternalDashHeadlightLevel(int msgInternalDashHeadlightLevel);
    void sigInternalDashHeadlightOff(const QString &msgInternalDashHeadlightOff);
    void sigInternalDashHeadlightOn(const QString &msgInternalDashHeadlightOn);
    void sigInternalDashLights(bool msgInternalDashLights);
    void sigInternalDashLockHood(const QString &msgInternalDashLockHood);
    void sigInternalDashLockLeftDoor(const QString &msgInternalDashLockLeftDoor);
    void sigInternalDashLockRightDoor(const QString &msgInternalDashLockRightDoor);
    void sigInternalDashLockTrunk(const QString &msgInternalDashLockTrunk);
    void sigInternalDashUnLockHood(const QString &msgInternalDashUnLockHood);
    void sigInternalDashUnLockLeftDoor(const QString &msgInternalDashUnLockLeftDoor);
    void sigInternalDashUnLockRightDoor(const QString &msgInternalDashUnLockRightDoor);
    void sigInternalDashUnLockTrunk(const QString &msgInternalDashUnLockTrunk);
    void sigInternalDashWiperBladeLevel(int msgInternalDashWiperBladeLevel);
    void sigInternalDashWiperBladeOff(const QString &msgInternalDashWiperBladeOff);
    void sigInternalDashWiperBladeOn(const QString &msgInternalDashWiperBladeOn);
    void sigMusicPlayerChangeMode(const QString &msgMusicPlayerChangeMode);
    void sigMusicPlayerNextTrack(const QString &msgMusicPlayerNextTrack);
    void sigMusicPlayerPauseTrack(const QString &msgMusicPlayerPauseTrack);
    void sigMusicPlayerPlaySongFromList(const QString &msgMusicPlayerPlayUrl, const QString &msgMusicPlayerPlayThumb, const QString &msgMusicPlayerPlayTitle);
    void sigMusicPlayerPreviousTrack(const QString &msgMusicPlayerPreviousTrack);
    void sigMusicPlayerStopTrack(const QString &msgMusicPlayerStopTrack);
    void sigNavigationChangeMapMode(const QString &msgNavigationChangeMapMode);
    void sigNavigationChangeMapStyle(const QString &msgNavigationChangeMapStyle);
    void sigNavigationFindNearbyPlacesType(const QString &msgNavigationFindNearbyPlacesType);
    void sigNavigationGetToLocation(const QString &msgNavigationGetToLocation);
    void sigNavigationShowHomeLocation(const QString &msgNavigationShowHomeLocation);
    void sigRadioPlayerChangeMode(const QString &msgRadioPlayerChangeMode);
    void sigRadioPlayerScanStations(const QString &msgRadioPlayerScanStations);
    void sigRadioPlayerSelectStation(int msgRadioPlayerSelectStation);
    void sigRadioPlayerTurnOff(const QString &msgRadioPlayerTurnOff);
    void sigRadioPlayerTurnOn(const QString &msgRadioPlayerTurnOn);
    void sigShowNotif(const QString &msgShowNotif);
};

#endif
