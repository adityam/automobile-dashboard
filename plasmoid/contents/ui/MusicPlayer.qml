import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.private.automobiledash 1.0 as AutoDash
import AudioProbe 1.0
import Mycroft 1.0 as Mycroft

ColumnLayout {
    id: musicPlayerRoot
    anchors.fill: parent
    property bool thumbnailVisible
    property bool titleVisible
    property var status
    property bool progressBar: true
    property var scCurrentSongUrl
    property var searchKeyword
    property var playerType
    property var scJsonModel
    property var lcJsonModel
    
    property var previousItemTitle
    property var previousItemUrl
    property var previousItemThumbnail
    property var nextItemTitle
    property var nextItemUrl
    property var nextItemThumbnail

    property var currentSelectedModel: playerType == "SoundCloud" ? scJsonModel : lcJsonModel

    function setPreviousNext(previousItemIndex, nextItemIndex) {
        if(previousItemIndex == -1){
            previousItemTitle = currentSelectedModel[0].title
            previousItemThumbnail = currentSelectedModel[0].thumbnail
            previousItemUrl = currentSelectedModel[0].url
            nextItemTitle = currentSelectedModel[nextItemIndex].title
            nextItemThumbnail = currentSelectedModel[nextItemIndex].thumbnail
            nextItemUrl = currentSelectedModel[nextItemIndex].url            
        } else {
            previousItemTitle = currentSelectedModel[previousItemIndex].title
            previousItemThumbnail = currentSelectedModel[previousItemIndex].thumbnail
            previousItemUrl = currentSelectedModel[previousItemIndex].url
            nextItemTitle = currentSelectedModel[nextItemIndex].title
            nextItemThumbnail = currentSelectedModel[nextItemIndex].thumbnail
            nextItemUrl = currentSelectedModel[nextItemIndex].url
        }
    }
    
    function searchScRequest(){
        if(typeof musicPlayerRoot.scJsonModel == "undefined") {
            Mycroft.MycroftController.sendText("search soundcloud for kiiara")
            console.log("here3")
        }
    }
    
    Connections {
        target: Mycroft.MycroftController
        onStatusChanged: {
            if(soundCloudPlayerType.checked){
                    playerType = "SoundCloud"
            } else {
                    playerType = "Local"
            }
            if(Mycroft.MycroftController.status == 1){
                Mycroft.MycroftController.sendText("search locally for kiiara")
                searchScRequest()
            }
        }
    }
     
    Connections {
        target: dashboard
        ignoreUnknownSignals: true

        onSendScResult: {
                scJsonModel = JSON.parse(msgScResult)
        }
        onSendLcResult: {
                lcJsonModel = JSON.parse(msgLcResult)
        }
        
        onSigMusicPlayerChangeMode: {
            if(msgMusicPlayerChangeMode == "Local"){
                playerType = "Local"
                soundCloudPlayerType.checked = false
                localPlayerType.checked = true
            } else if(msgMusicPlayerChangeMode == "Online") {
                playerType = "SoundCloud"
                localPlayerType.checked = false
                soundCloudPlayerType.checked = true
            }
        }
        onSigMusicPlayerNextTrack: {
            albumimg.source = nextItemThumbnail
            root.albumArt = nextItemThumbnail
            songtitle.text = nextItemTitle
            root.trackTitle = songtitle.text
            playButton.focus = false
            nextButton.focus = false
            root.playURL(nextItemUrl)
            
        }
        onSigMusicPlayerPauseTrack: {
            root.pausePlayer()
        }
        onSigMusicPlayerPreviousTrack: {
            albumimg.source = previousItemThumbnail
            root.albumArt = previousItemThumbnail
            songtitle.text = previousItemTitle
            root.trackTitle = songtitle.text
            playButton.focus = false
            previousButton.focus = false
            root.playURL(previousItemUrl)
        }
        onSigMusicPlayerStopTrack: {
            root.stopPlayer()
            root.trackTitle = ""
        }
        onSigMusicPlayerPlaySongFromList: {
            songtitle.text = msgMusicPlayerPlayTitle
            albumimg.source = msgMusicPlayerPlayThumb
            root.albumArt = msgMusicPlayerPlayThumb
            root.trackTitle = msgMusicPlayerPlayTitle
            root.playFromURL(msgMusicPlayerPlayUrl)
        }
    }

    onSearchKeywordChanged: {
        Mycroft.MycroftController.sendText("search soundcloud for " + searchKeyword)
    }

    RowLayout {
        Layout.fillWidth: true
        Layout.preferredHeight: parent.height / 2
        
        Item {
            Layout.fillHeight: true
            Layout.preferredWidth: parent.width / 3

            PlasmaComponents.ButtonColumn {
                id: musicPlayerTypeGroup
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: albumimg.top
                spacing: Kirigami.Units.smallSpacing

                PlasmaComponents.Button {
                    id: soundCloudPlayerType
                    checkable: true
                    checked: false
                    text: "Online Media"
                    onClicked: {
                        playerType = "SoundCloud"
                    }
                }
                PlasmaComponents.Button {
                    id: localPlayerType
                    checkable: true
                    checked: true
                    text: "Local Media"
                    onClicked: {
                        playerType = "Local"
                        Mycroft.MycroftController.sendText("search locally for kiiara")
                    }
                }
            }

            Image {
                id: albumimg
                fillMode: Image.PreserveAspectFit
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: parent.height / 1.5
                source: "https://jamielkyte.files.wordpress.com/2010/07/example-last-ones-standing-official-single-cover.jpg"
            }
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: parent.height
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 0.5

            RowLayout {
                id: searchFieldRow
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.topMargin:  Kirigami.Units.largeSpacing
                anchors.leftMargin: Kirigami.Units.largeSpacing
                anchors.rightMargin: Kirigami.Units.largeSpacing
                height: Kirigami.Units.gridUnit * 3
                spacing: -0
                PlasmaComponents.TextField {
                    id: songSearchField
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    onAccepted: {
                        musicPlayerRoot.searchKeyword = songSearchField.text
                    }
                }
                PlasmaComponents.Button {
                    id: songSearchButton
                    Layout.preferredWidth: Kirigami.Units.gridUnit * 4
                    Layout.fillHeight: true
                    text: "Search"
                    onClicked: {
                        musicPlayerRoot.searchKeyword = songSearchField.text
                    }
                }
            }

            Kirigami.CardsListView {
                id: scListView
                anchors.top: searchFieldRow.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.bottomMargin: Kirigami.Units.largeSpacing
                clip: true
                model: currentSelectedModel
                delegate: Kirigami.AbstractCard {
                    showClickFeedback: true
                    Layout.fillWidth: true
                    implicitHeight: delegateItem.implicitHeight + Kirigami.Units.largeSpacing * 3
                    //highlighted: modelData.url == player.source ? 1 : 0
                    property int currentItemIndex: index
                    property int nextItemIndex: index + 1
                    property int previousItemIndex: index - 1
                    
                    onHighlightedChanged: {
                        if(highlighted){
                            setPreviousNext(previousItemIndex, nextItemIndex)
                        }
                    }
                    
                    contentItem: Item {
                        implicitWidth: parent.implicitWidth
                        implicitHeight: parent.implicitHeight
                        z: 1000

                        RowLayout {
                            id: delegateItem
                            anchors {
                                left: parent.left
                                right: parent.right
                                top: parent.top
                            }
                            spacing: Kirigami.Units.largeSpacing

                            Image {
                                id: videoImage
                                source: modelData.thumbnail
                                Layout.preferredHeight: Kirigami.Units.gridUnit * 3
                                Layout.preferredWidth: Kirigami.Units.gridUnit * 3
                                fillMode: Image.Stretch
                            }

                            Kirigami.Separator {
                                Layout.fillHeight: true
                                color: Kirigami.Theme.linkColor
                            }

                            Controls.Label {
                                id: videoLabel
                                Layout.fillWidth: true
                                text: modelData.title
                                wrapMode: Text.WordWrap
                            }
                        }
                    }
                    onClicked: {
                        songtitle.text = modelData.title
                        albumimg.source = modelData.thumbnail
                        root.albumArt = modelData.thumbnail
                        root.trackTitle = songtitle.text
                        root.playFromURL(modelData.url)
                        setPreviousNext(previousItemIndex, nextItemIndex)
                    }
                }
            }
        }
    }

    GridLayout {
        Layout.preferredHeight: parent.height / 2
        columns: 2
        
        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            spacing: Kirigami.Units.largeSpacing

            RowLayout {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.alignment: Qt.AlignHCenter
                spacing: Kirigami.Units.largeSpacing
                
                Controls.Button {
                    id: previousButton
                    Layout.minimumWidth: Kirigami.Units.iconSizes.smallMedium
                    Layout.minimumHeight: width
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumWidth: Kirigami.Units.gridUnit * 3
                    Layout.maximumHeight: width
                    focus: false
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: previousButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                        radius: 10
                    }
                    contentItem: Kirigami.Icon {
                        source: "media-seek-backward"
                        anchors.centerIn: parent
                        width: Kirigami.Units.iconSizes.small - Kirigami.Units.largeSpacing
                        height: Kirigami.Units.iconSizes.small - Kirigami.Units.largeSpacing
                    }
                    onClicked: {
                        albumimg.source = previousItemThumbnail
                        root.albumArt = previousItemThumbnail
                        //player.source = previousItemUrl
                        songtitle.text = previousItemTitle
                        playButton.focus = false
                        previousButton.focus = false
                        //player.play()
                        root.playURL(previousItemUrl)
                    }
                }
                
                Controls.Button {
                    id: stopButton
                    Layout.minimumWidth: Kirigami.Units.iconSizes.medium
                    Layout.minimumHeight: width
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumWidth: Kirigami.Units.gridUnit * 4
                    Layout.maximumHeight: width
                    focus: false
                    visible: root.playbackState === MediaPlayer.PlayingState ? 1 : 0
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: playButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                        radius: 10
                    }
                    contentItem: Kirigami.Icon {
                        source: "media-playback-stop"
                        anchors.centerIn: parent
                        width: Kirigami.Units.iconSizes.small
                        height: Kirigami.Units.iconSizes.small
                    }
                    onClicked: {
                        root.stopPlayer();
                    }
                }

                Controls.Button {
                    id: playButton
                    Layout.minimumWidth: Kirigami.Units.iconSizes.medium
                    Layout.minimumHeight: width
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumWidth: Kirigami.Units.gridUnit * 4
                    Layout.maximumHeight: width
                    focus: false
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: playButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                        radius: 10
                    }
                    contentItem: Kirigami.Icon {
                        source: root.playbackState === MediaPlayer.PlayingState ? "media-playback-pause" : "media-playback-start"
                        anchors.centerIn: parent
                        width: Kirigami.Units.iconSizes.small
                        height: Kirigami.Units.iconSizes.small
                    }
                    onClicked: {
                        root.playbackState === MediaPlayer.PlayingState ? root.pausePlayer() : root.continuePlayer()
                        playButton.focus = false
                    }
                }

                Controls.Button {
                    id: nextButton
                    Layout.minimumWidth: Kirigami.Units.iconSizes.smallMedium
                    Layout.minimumHeight: width
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.maximumWidth: Kirigami.Units.gridUnit * 3
                    Layout.maximumHeight: width
                    focus: false
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: nextButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                        radius: 10
                    }
                    contentItem: Kirigami.Icon {
                        source: "media-seek-forward"
                        anchors.centerIn: parent
                        width: Kirigami.Units.iconSizes.small - Kirigami.Units.largeSpacing
                        height: Kirigami.Units.iconSizes.small - Kirigami.Units.largeSpacing
                    }
                    onClicked: {
                        albumimg.source = nextItemThumbnail
                        root.albumArt = nextItemThumbnail
                        //player.source = nextItemUrl
                        songtitle.text = nextItemTitle
                        playButton.focus = false
                        nextButton.focus = false
                        //player.play()
                        root.playURL(nextItemUrl)
                    }
                }
            }

            RowLayout {
                spacing: Kirigami.Units.smallSpacing
                Layout.fillWidth: true
                visible: musicPlayerRoot.progressBar ? 1 : 0
                enabled: musicPlayerRoot.progressBar ? 1 : 0

                Item {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Row {
                        //anchors.fill: parent
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 4
                        z: -10
                        Repeater {
                            model: root.soundModelLength
                            delegate: Rectangle {
                                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                                width: Kirigami.Units.gridUnit * 1.25
                                radius: 3
                                opacity: root.playbackState === MediaPlayer.PlayingState ? 1 : 0
                                anchors.verticalCenter: parent.verticalCenter
                                height: 15 + root.spectrum[modelData] * parent.height
                                Behavior on height {
                                    NumberAnimation {
                                        duration: 150
                                        easing.type: Easing.Linear
                                    }
                                }
                                Behavior on opacity {
                                    NumberAnimation{
                                        duration: 1500 + root.spectrum[modelData] * parent.height
                                        easing.type: Easing.Linear
                                    }
                                }
                            }
                        }
                    }
                    Item {
                        anchors.fill: parent
                        z: 10
                        Controls.Slider {
                            id: seekableslider
                            //to: player.duration
                            anchors.left: parent.left
                            anchors.right: parent.right
                            anchors.verticalCenter: parent.verticalCenter

                            onPressedChanged: {
                                    root.seekPlayer(value)
                            }

                            handle: Rectangle {
                                x: seekableslider.leftPadding + seekableslider.visualPosition * (seekableslider.availableWidth - width)
                                y: seekableslider.topPadding + seekableslider.availableHeight / 2 - height / 2
                                implicitWidth: 26
                                implicitHeight: 26
                                radius: 13
                                color: seekableslider.pressed ? "#f0f0f0" : "#f6f6f6"
                                border.color: "#bdbebf"
                            }

                            Connections {
                                target: root
                                onTrackLengthChanged: {
                                    seekableslider.to = root.trackLength
                                }
                                onTrackPositionChanged: {
                                    seekableslider.value = root.trackPosition
                                }
                            }
                        }
                    }
                }

                Controls.Label {
                    id: positionLabel
                    //readonly property int minutes: Math.floor(root.trackPosition / 60000)
                    //readonly property int seconds: Math.round((root.trackPosition % 60000) / 1000)
                    //text: Qt.formatTime(new Date(0, 0, 0, 0, minutes, seconds), qsTr("mm:ss"))
                }
            }

            Kirigami.Heading {
                id: songtitle
                text: root.trackTitle
                level: 2
                Layout.fillWidth: true
                Layout.leftMargin: Kirigami.Units.largeSpacing
                Layout.alignment: Qt.AlignBottom
                elide: Text.ElideRight
                font.capitalization: Font.Capitalize
                font.italic: true
                font.bold: true
            }
        }
        
        ColumnLayout {
            Layout.preferredWidth: musicPlayerRootVolumeCtrl.width
            Layout.fillHeight: true
            Layout.topMargin: Kirigami.Units.largeSpacing
            Layout.bottomMargin: Kirigami.Units.largeSpacing
            Layout.leftMargin: Kirigami.Units.largeSpacing
            Controls.Slider {
                id: musicPlayerRootVolumeCtrl
                from: 0
                value: 42
                to: 100
                orientation: Qt.Vertical
                Layout.alignment: Qt.AlignHCenter
                Layout.preferredWidth: Kirigami.Units.gridUnit * 3
                Layout.fillHeight: true
                
                background: Rectangle {
                    width:  parent.width / 3
                    height: parent.height
                    radius: 10
                    anchors.centerIn: parent
                    color: Kirigami.Theme.linkColor
                    
                    Rectangle {
                        width: parent.width
                        height: musicPlayerRootVolumeCtrl.visualPosition * parent.height
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.1)
                        radius: 10
                    }
                }
                
                handle: Rectangle {
                        x: musicPlayerRootVolumeCtrl.width / 3.25
                        y: musicPlayerRootVolumeCtrl.topPadding + musicPlayerRootVolumeCtrl.visualPosition * (musicPlayerRootVolumeCtrl.availableHeight - height)
                        implicitWidth: 26
                        implicitHeight: 26
                        radius: 13
                        color: musicPlayerRootVolumeCtrl.pressed ? "#f0f0f0" : "#f6f6f6"
                        border.color: "#bdbebf"
                    }
                }
            
                Kirigami.Heading {
                text: "Vol:" + musicPlayerRootVolumeCtrl.value.toFixed(0) + "%"
                Layout.alignment: Qt.AlignHCenter
                font.italic: true
                level: 3
            }
        }
    }
}
