import QtQuick 2.9
import QtQml.Models 2.2
import QtQuick.Controls 2.2 as Controls
import QtQuick.Layouts 1.3
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.kirigami 2.5 as Kirigami
import Mycroft 1.0 as Mycroft

Item {
    id: page
    property alias cfg_speakBar: speakBarSwitch.checked
    
    ColumnLayout{
    Layout.fillWidth: true

        PlasmaComponents.Switch {
                id: speakBarSwitch
                Layout.fillWidth: true
                text: i18n("Enable Speak Bar")
                checked: true
        }
    }
}
