import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import Mycroft 1.0 as Mycroft

ColumnLayout {
    id: musicRoot
    anchors.fill: parent

    PlasmaComponents.TabBar {
        id: musicSheetTabBar
        Layout.alignment: Qt.AlignHCenter
        Layout.fillWidth: true
        Layout.preferredHeight: Kirigami.Units.gridUnit * 2
        PlasmaComponents.TabButton {
            id: radioTab
            text: "Radio"
        }
        PlasmaComponents.TabButton {
            id: audioTab
            text: "Audio"
        }
    }

    Controls.Frame {
        Layout.fillHeight: true
        Layout.fillWidth: true
        Layout.margins: Kirigami.Units.largeSpacing
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
            border.color: Kirigami.Theme.disabledTextColor
            radius: 2
        }

        RadioPlayer {
            visible: musicSheetTabBar.currentTab == radioTab
        }

        MusicPlayer {
             visible: musicSheetTabBar.currentTab == audioTab
        }
    }
}
