import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.private.automobiledash 1.0 as AutoDash
import AudioProbe 1.0
import Mycroft 1.0 as Mycroft

ColumnLayout {
    id: musicPlayerRoot
    anchors.fill: parent
    property bool thumbnailVisible
    property bool titleVisible
    property var status
    property bool progressBar: true
    property var scCurrentSongUrl
    property var searchKeyword
    property var playerType
    property var scJsonModel
    property var lcJsonModel
    
    property var previousItemTitle
    property var previousItemUrl
    property var previousItemThumbnail
    property var nextItemTitle
    property var nextItemUrl
    property var nextItemThumbnail

    RowLayout {
        anchors.fill: parent

        Item{
            Layout.preferredWidth: Kirigami.Units.largeSpacing
        }
        
        Item {
            Layout.preferredHeight: Kirigami.Units.gridUnit * 2
            Layout.preferredWidth: Kirigami.Units.gridUnit * 2
            Layout.alignment: Qt.AlignLeft

            Image {
                id: albumimg
                fillMode: Image.PreserveAspectCrop
                anchors.fill: parent
                source: root.albumArt
            }
        }
        
        Controls.Button {
            id: previousButton
            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
            focus: false
            background: Rectangle {
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: previousButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 10
            }
            contentItem: Kirigami.Icon {
                source: "media-seek-backward"
                anchors.centerIn: parent
                width: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
                height: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
            }
            onClicked: {
                albumimg.source = previousItemThumbnail
                //player.source = previousItemUrl
                songtitle.text = previousItemTitle
                playButton.focus = false
                previousButton.focus = false
                //player.play()
                root.playPrevious()
            }
        }

        Controls.Button {
            id: playButton
            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
            focus: false
            background: Rectangle {
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: playButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 10
            }
            contentItem: Kirigami.Icon {
                source: root.playbackState === MediaPlayer.PlayingState ? "media-playback-pause" : "media-playback-start"
                anchors.centerIn: parent
                width: Kirigami.Units.iconSizes.smallMedium
                height: Kirigami.Units.iconSizes.smallMedium
            }
            onClicked: {
                root.playbackState === MediaPlayer.PlayingState ? root.pausePlayer() : root.continuePlayer()
                playButton.focus = false
            }
        }

        Controls.Button {
            id: nextButton
            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
            focus: false
            background: Rectangle {
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: nextButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 10
            }
            contentItem: Kirigami.Icon {
                source: "media-seek-forward"
                anchors.centerIn: parent
                width: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
                height: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
            }
            onClicked: {
                albumimg.source = nextItemThumbnail
                songtitle.text = nextItemTitle
                playButton.focus = false
                nextButton.focus = false
                root.playNext()
            }
        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Row {
                //anchors.fill: parent
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 4
                z: -10
                Repeater {
                    model: root.soundModelLength
                    delegate: Rectangle {
                        color: Kirigami.Theme.linkColor
                        width: Kirigami.Units.gridUnit * 1
                        radius: 3
                        opacity: root.playbackState === MediaPlayer.PlayingState ? 1 : 0
                        anchors.verticalCenter: parent.verticalCenter
                        height: root.spectrum[modelData] * parent.height - 2
                        Behavior on height {
                            NumberAnimation {
                                duration: 150
                                easing.type: Easing.Linear
                            }
                        }
                        Behavior on opacity {
                            NumberAnimation{
                                duration: 1500 + root.spectrum[modelData] * parent.height
                                easing.type: Easing.Linear
                            }
                        }
                    }
                }
            }
            Item {
                anchors.fill: parent
                z: 10
                Controls.Slider {
                    id: seekableslider
                    //to: player.duration
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    property bool sync: false

                    onPressedChanged: {
                            root.seekPlayer(value)
                    }

                    handle: Rectangle {
                        x: seekableslider.leftPadding + seekableslider.visualPosition * (seekableslider.availableWidth - width)
                        y: seekableslider.topPadding + seekableslider.availableHeight / 2 - height / 2
                        implicitWidth: 26
                        implicitHeight: 26
                        radius: 13
                        color: seekableslider.pressed ? "#f0f0f0" : "#f6f6f6"
                        border.color: "#bdbebf"
                    }

                    Connections {
                        target: root
                        onTrackLengthChanged: {
                            seekableslider.to = root.trackLength
                        }
                        onTrackPositionChanged: {
                            seekableslider.value = root.trackPosition
                        }
                    }
                }
            }
        }
        
        Controls.Button {
            id: stopButton
            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
            focus: false
            background: Rectangle {
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: previousButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 10
            }
            contentItem: Kirigami.Icon {
                source: "media-playback-stop"
                anchors.centerIn: parent
                width: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
                height: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
            }
            onClicked: {
                root.stopPlayer();
            }
        }
        
        Controls.Button {
            id: volButton
            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
            focus: false
            background: Rectangle {
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: previousButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 10
            }
            contentItem: Kirigami.Icon {
                source: "player-volume"
                anchors.centerIn: parent
                width: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
                height: Kirigami.Units.iconSizes.smallMedium - Kirigami.Units.largeSpacing
            }
            onClicked: {
                console.log("nothing")
            }
        }
        
        Item{
            Layout.preferredWidth: Kirigami.Units.largeSpacing
        }
    }
}
