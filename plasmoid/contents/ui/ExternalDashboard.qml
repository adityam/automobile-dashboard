import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import Mycroft 1.0 as Mycroft

ColumnLayout {
    Layout.fillWidth: true
    
    Connections {
        target: dashboard
        ignoreUnknownSignals: true
        onSigInternalDashLockHood: {
            rectDoorTopIcon.source = "document-encrypt"
            topDoorBoxStatus.text = "Locked"
            topDoorBoxStatus.color = "Red"
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The hood has been locked"})
        }
        onSigInternalDashUnLockHood: {
            rectDoorTopIcon.source = "document-decrypt"
            topDoorBoxStatus.text = "Unlocked"
            topDoorBoxStatus.color = Kirigami.Theme.linkColor
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The hood has been unlocked"})
        }
        onSigInternalDashLockTrunk: {
            rectDoorBottomIcon.source = "document-encrypt"
            bottomDoorBoxStatus.text = "Locked"
            bottomDoorBoxStatus.color = "Red"
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The trunk has been locked"})
        }
        onSigInternalDashUnLockTrunk: {
            rectDoorBottomIcon.source = "document-decrypt"
            bottomDoorBoxStatus.text = "Unlocked"
            bottomDoorBoxStatus.color = Kirigami.Theme.linkColor
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The trunk has been unlocked"})
        }
        onSigInternalDashLockLeftDoor: {
            rectDoorLeftIcon.source = "document-encrypt"
            leftDoorBoxStatus.text = "Locked"
            leftDoorBoxStatus.color = "Red"
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The left door has been locked"})
        }
        onSigInternalDashUnLockLeftDoor: {
            rectDoorLeftIcon.source = "document-decrypt"
            leftDoorBoxStatus.text = "Unlocked"
            leftDoorBoxStatus.color = Kirigami.Theme.linkColor
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The left door has been unlocked"})
        }
        onSigInternalDashLockRightDoor: {
            rectDoorRightIcon.source = "document-encrypt"
            rightDoorBoxStatus.text = "Locked"
            rightDoorBoxStatus.color = "Red"
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The right door has been locked"})
        }
        onSigInternalDashUnLockRightDoor: {
            rectDoorRightIcon.source = "document-decrypt"
            rightDoorBoxStatus.text = "Unlocked"
            rightDoorBoxStatus.color = Kirigami.Theme.linkColor
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The right door has been unlocked"})
        }
        onSigInternalDashHeadlightOn: {
            headlightColorStatus.color = "Green"
            headlightPowerStatus.text = "On"
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The headlights have been turned on"})
        }
        onSigInternalDashHeadlightOff: {
            headlightColorStatus.color = "Red"
            headlightPowerStatus.text = "Off"
            Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The headlights have been turned off"})            
        }
        onSigInternalDashHeadlightLevel: {
            console.log("inHeadLightLevel");
        }
        onSigInternalDashWiperBladeOn: {
            console.log("inWiperBladeOn");
        }
        onSigInternalDashWiperBladeOff: {
            console.log("inWiperBladeOff");
        }
        onSigInternalDashWiperBladeLevel: {
            console.log("inWiperBladeLevel");
        }
    }

    RowLayout {
        Layout.fillWidth:  true
        Layout.preferredHeight: Kirigami.Units.gridUnit * 2

        Rectangle {
            Layout.preferredWidth: Kirigami.Units.gridUnit * 12
            Layout.preferredHeight: Kirigami.Units.gridUnit * 2
            Layout.alignment: Qt.AlignTop
            Layout.topMargin: Kirigami.Units.largeSpacing * 4
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 1
            radius: 2

            Kirigami.Heading {
                id: headlightLabelHeading
                text: "Headlights"
                color: Kirigami.Theme.linkColor
                level: 2
                anchors.left: parent.left
                anchors.leftMargin: Kirigami.Units.largeSpacing
                anchors.verticalCenter: parent.verticalCenter
            }

            Kirigami.Separator {
                id: headlightSept
                width: 1
                height: parent.height
                anchors.left: headlightLabelHeading.right
                anchors.leftMargin: Kirigami.Units.largeSpacing
            }

            Controls.Button {
                id: headlightButton
                anchors.left: headlightSept.right
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom

                onClicked: {
                    if(headlightPowerStatus.text == "Off"){
                        headlightColorStatus.color = "Green"
                        headlightPowerStatus.text = "On"
                    } else {
                        headlightColorStatus.color = "Red"
                        headlightPowerStatus.text = "Off"
                    }
                }

                background: Rectangle {
                    color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                    border.color: headlightButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                    border.width: 1
                }
                contentItem: RowLayout {
                    spacing: -Kirigami.Units.gridUnit * 2
                    Rectangle {
                        id: headlightColorStatus
                        color: "Red"
                        Layout.preferredWidth: Kirigami.Units.iconSizes.small
                        Layout.preferredHeight: Kirigami.Units.iconSizes.small
                        radius: 100
                        Layout.leftMargin: Kirigami.Units.largeSpacing
                    }
                    Controls.Label {
                        id: headlightPowerStatus
                        text: "Off"
                    }
                }
            }
        }
        Item {
            id: headlightSlider
            Layout.fillHeight: true
            Layout.preferredWidth: Kirigami.Units.gridUnit * 4
            visible: headlightPowerStatus.text == "On" ? 1 : 0
            Controls.Slider {
                from: 1
                to: 3
                value: 1
                stepSize: 1
                snapMode: Controls.Slider.SnapAlways
                anchors.verticalCenter: parent.verticalCenter
                anchors.verticalCenterOffset: Kirigami.Units.largeSpacing * 2
                Kirigami.Theme.colorGroup: Kirigami.Theme.Disabled
            }
        }
    }
    
    RowLayout {
        spacing: 0

        RowLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                id: rectDoorLeftLabel
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 0.5
                Layout.alignment: Qt.AlignAbsolute
                Layout.preferredWidth: Kirigami.Units.gridUnit * 5
                Layout.preferredHeight: Kirigami.Units.gridUnit * 3
                Kirigami.Heading {
                    id: leftDoorBoxHeading
                    level: 3
                    text: "Left Door"
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Kirigami.Separator {
                    id: leftDoorBoxSept
                    width: parent.width
                    height: 1
                    anchors.top: leftDoorBoxHeading.bottom
                    anchors.topMargin: Kirigami.Units.smallSpacing
                }
                Controls.Label {
                    id: leftDoorBoxStatus
                    text: "Unlocked"
                    color: Kirigami.Theme.linkColor
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: Kirigami.Units.largeSpacing
                }
            }

            Rectangle {
                id: rectDoorLeft
                Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                radius: 100
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 0.5
                Layout.alignment: Qt.AlignAbsolute
                Kirigami.Icon {
                    id: rectDoorLeftIcon
                    anchors.centerIn: parent
                    source: "document-decrypt"
                    width: Kirigami.Units.iconSizes.small
                    height: Kirigami.Units.iconSizes.small
                }
                z: 1000
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        rectDoorLeft.color = Kirigami.Theme.disabledTextColor
                        rectDoorLeft.border.color = Kirigami.Theme.textColor
                    }
                    onExited: {
                        rectDoorLeft.color = Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        rectDoorLeft.border.color = Kirigami.Theme.disabledTextColor
                    }
                    onClicked: {
                        if(rectDoorLeftIcon.source == "document-decrypt"){
                            rectDoorLeftIcon.source = "document-encrypt"
                            leftDoorBoxStatus.text = "Locked"
                            leftDoorBoxStatus.color = "Red"
                        } else {
                            rectDoorLeftIcon.source = "document-decrypt"
                            leftDoorBoxStatus.text = "Unlocked"
                            leftDoorBoxStatus.color = Kirigami.Theme.linkColor
                        }
                    }
                }
            }

            Image {
                Layout.fillHeight: true
                Layout.preferredWidth: Kirigami.Units.gridUnit * 14
                source: "../images/CarExternal.png"
                fillMode: Image.PreserveAspectFit
                
                RowLayout {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.horizontalCenterOffset: Kirigami.Units.gridUnit * 2.75
                    anchors.top: parent.top
                    anchors.topMargin: Kirigami.Units.gridUnit * 2.5

                    Rectangle {
                        id: rectDoorTop
                        Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                        Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        radius: 100
                        border.color: Kirigami.Theme.disabledTextColor
                        border.width: 0.5
                        Layout.alignment: Qt.AlignAbsolute
                        Kirigami.Icon {
                            id: rectDoorTopIcon
                            anchors.centerIn: parent
                            source: "document-decrypt"
                            width: Kirigami.Units.iconSizes.small
                            height: Kirigami.Units.iconSizes.small
                        }
                        z: 1000
                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            onEntered: {
                                rectDoorTop.color = Kirigami.Theme.disabledTextColor
                                rectDoorTop.border.color = Kirigami.Theme.textColor
                            }
                            onExited: {
                                rectDoorTop.color = Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                                rectDoorTop.border.color = Kirigami.Theme.disabledTextColor
                            }
                            onClicked: {
                                if(rectDoorTopIcon.source == "document-decrypt"){
                                    rectDoorTopIcon.source = "document-encrypt"
                                    topDoorBoxStatus.text = "Locked"
                                    topDoorBoxStatus.color = "Red"
                                } else {
                                    rectDoorTopIcon.source = "document-decrypt"
                                    topDoorBoxStatus.text = "Unlocked"
                                    topDoorBoxStatus.color = Kirigami.Theme.linkColor
                                }
                            }
                        }
                    }
                    
                    Rectangle {
                        id: rectDoorTopLabel
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: Kirigami.Theme.disabledTextColor
                        border.width: 0.5
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: Kirigami.Units.gridUnit * 5
                        Layout.preferredHeight: Kirigami.Units.gridUnit * 3
                        Kirigami.Heading {
                            id: topDoorBoxHeading
                            level: 3
                            text: "Hood"
                            anchors.top: parent.top
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Kirigami.Separator {
                            id: topDoorBoxSept
                            width: parent.width
                            height: 1
                            anchors.top: topDoorBoxHeading.bottom
                            anchors.topMargin: Kirigami.Units.smallSpacing
                        }
                        Controls.Label {
                            id: topDoorBoxStatus
                            text: "Unlocked"
                            color: Kirigami.Theme.linkColor
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: Kirigami.Units.largeSpacing
                        }
                    }
                }
                
                RowLayout {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.horizontalCenterOffset: Kirigami.Units.gridUnit * 2.75
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: Kirigami.Units.gridUnit * 3.5

                    Rectangle {
                        id: rectDoorBottom
                        Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                        Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        radius: 100
                        border.color: Kirigami.Theme.disabledTextColor
                        border.width: 0.5
                        Layout.alignment: Qt.AlignAbsolute
                        Kirigami.Icon {
                            id: rectDoorBottomIcon
                            anchors.centerIn: parent
                            source: "document-decrypt"
                            width: Kirigami.Units.iconSizes.small
                            height: Kirigami.Units.iconSizes.small
                        }
                        z: 1000
                        MouseArea {
                            anchors.fill: parent
                            hoverEnabled: true
                            onEntered: {
                                rectDoorBottom.color = Kirigami.Theme.disabledTextColor
                                rectDoorBottom.border.color = Kirigami.Theme.textColor
                            }
                            onExited: {
                                rectDoorBottom.color = Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                                rectDoorBottom.border.color = Kirigami.Theme.disabledTextColor
                            }
                            onClicked: {
                                if(rectDoorBottomIcon.source == "document-decrypt"){
                                    rectDoorBottomIcon.source = "document-encrypt"
                                    bottomDoorBoxStatus.text = "Locked"
                                    bottomDoorBoxStatus.color = "Red"
                                } else {
                                    rectDoorBottomIcon.source = "document-decrypt"
                                    bottomDoorBoxStatus.text = "Unlocked"
                                    bottomDoorBoxStatus.color = Kirigami.Theme.linkColor
                                }
                            }
                        }
                    }
                    
                    Rectangle {
                        id: rectDoorBottomLabel
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: Kirigami.Theme.disabledTextColor
                        border.width: 0.5
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredWidth: Kirigami.Units.gridUnit * 5
                        Layout.preferredHeight: Kirigami.Units.gridUnit * 3
                        Kirigami.Heading {
                            id: bottomDoorBoxHeading
                            level: 3
                            text: "Trunk"
                            anchors.top: parent.top
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Kirigami.Separator {
                            id: bottomDoorBoxSept
                            width: parent.width
                            height: 1
                            anchors.top: bottomDoorBoxHeading.bottom
                            anchors.topMargin: Kirigami.Units.smallSpacing
                        }
                        Controls.Label {
                            id: bottomDoorBoxStatus
                            text: "Unlocked"
                            color: Kirigami.Theme.linkColor
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: Kirigami.Units.largeSpacing
                        }
                    }
                }
            }
            
            Rectangle {
                id: rectDoorRight
                Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                radius: 100
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 0.5
                Layout.alignment: Qt.AlignAbsolute
                Kirigami.Icon {
                    id: rectDoorRightIcon
                    anchors.centerIn: parent
                    source: "document-decrypt"
                    width: Kirigami.Units.iconSizes.small
                    height: Kirigami.Units.iconSizes.small
                }
                z: 1000
                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        rectDoorRight.color = Kirigami.Theme.disabledTextColor
                        rectDoorRight.border.color = Kirigami.Theme.textColor
                    }
                    onExited: {
                        rectDoorRight.color = Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        rectDoorRight.border.color = Kirigami.Theme.disabledTextColor
                    }
                    onClicked: {
                        if(rectDoorRightIcon.source == "document-decrypt"){
                            rectDoorRightIcon.source = "document-encrypt"
                            rightDoorBoxStatus.text = "Locked"
                            rightDoorBoxStatus.color = "Red"
                        } else {
                            rectDoorRightIcon.source = "document-decrypt"
                            rightDoorBoxStatus.text = "Unlocked"
                            rightDoorBoxStatus.color = Kirigami.Theme.linkColor
                        }
                    }
                }
            }
            Rectangle {
                id: rectDoorRightLabel
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 0.5
                Layout.alignment: Qt.AlignAbsolute
                Layout.preferredWidth: Kirigami.Units.gridUnit * 5
                Layout.preferredHeight: Kirigami.Units.gridUnit * 3
                Kirigami.Heading {
                    id: rightDoorBoxHeading
                    level: 3
                    text: "Right Door"
                    anchors.top: parent.top
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Kirigami.Separator {
                    id: rightDoorBoxSept
                    width: parent.width
                    height: 1
                    anchors.top: rightDoorBoxHeading.bottom
                    anchors.topMargin: Kirigami.Units.smallSpacing
                }
                Controls.Label {
                    id: rightDoorBoxStatus
                    text: "Unlocked"
                    color: Kirigami.Theme.linkColor
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: Kirigami.Units.largeSpacing
                }
            }
        }

        ColumnLayout {
            Layout.fillHeight: true
            Layout.preferredWidth: Kirigami.Units.gridUnit * 11
            spacing: Kirigami.Units.largeSpacing * 2
            Layout.leftMargin: Kirigami.Units.gridUnit * 3
            Layout.alignment: Qt.AlignRight

            Rectangle {
                id: speedRect
                Layout.preferredWidth: Kirigami.Units.gridUnit * 10
                Layout.preferredHeight: Kirigami.Units.gridUnit * 8
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 2

                ColumnLayout {
                    anchors.fill: parent
                    spacing: Kirigami.Units.smallSpacing

                    Kirigami.Heading {
                        id: speedLabel
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredHeight: Kirigami.Units.gridUnit * 0.75
                        text: "Speed"
                        color: Kirigami.Theme.linkColor
                        level: 1
                    }

                    Rectangle {
                        color: Kirigami.Theme.disabledTextColor
                        Layout.preferredWidth: parent.width
                        Layout.preferredHeight: 1
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                        Kirigami.Icon {
                            Layout.preferredWidth: Kirigami.Units.iconSizes.large
                            Layout.preferredHeight: width
                            source: "speedometer"
                        }
                        Kirigami.Heading {
                            level: 1
                            Layout.alignment:  Qt.AlignVCenter
                            text: currentSpeed + " MPH"
                        }
                    }
                }
            }

            Rectangle {
                id: fuelRect
                Layout.preferredWidth: Kirigami.Units.gridUnit * 10
                Layout.preferredHeight: Kirigami.Units.gridUnit * 8
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 2

                ColumnLayout {
                    anchors.fill: parent

                    Kirigami.Heading {
                        id: fuelLevel
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredHeight: Kirigami.Units.gridUnit * 0.75
                        text: "Fuel"
                        color: Kirigami.Theme.linkColor
                        level: 1
                    }

                    Rectangle {
                        color: Kirigami.Theme.disabledTextColor
                        Layout.preferredWidth: parent.width
                        Layout.preferredHeight: 1
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                        Kirigami.Icon {
                            Layout.preferredWidth: Kirigami.Units.iconSizes.large
                            Layout.preferredHeight: width
                            source: "color-fill"
                        }
                        Kirigami.Heading {
                            level: 2
                            Layout.alignment:  Qt.AlignVCenter
                            text: "5.5L"
                        }
                    }
                }
            }
            Rectangle {
                id: switchBoxExternal
                Layout.preferredWidth: Kirigami.Units.gridUnit * 10
                Layout.preferredHeight: Kirigami.Units.gridUnit * 8
                color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                border.color: Kirigami.Theme.disabledTextColor
                border.width: 1
                radius: 2

                ColumnLayout {
                    anchors.fill: parent

                    Kirigami.Heading {
                        id: switchBoxWiperBlade
                        Layout.alignment: Qt.AlignHCenter
                        Layout.preferredHeight: Kirigami.Units.gridUnit * 0.5
                        text: "Wiperblade"
                        color: Kirigami.Theme.linkColor
                        level: 1
                    }

                    Rectangle {
                        color: Kirigami.Theme.disabledTextColor
                        Layout.preferredWidth: parent.width
                        Layout.preferredHeight: 1
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                        Rectangle {
                            Layout.preferredWidth: Kirigami.Units.iconSizes.small
                            Layout.preferredHeight: Kirigami.Units.iconSizes.small
                            radius: 100
                            color: "Green"
                        }

                        Kirigami.Heading {
                            level: 2
                            Layout.alignment:  Qt.AlignVCenter
                            text: "ON"
                        }

                        Repeater {
                            model: 4
                            delegate: Rectangle {
                                color: Kirigami.Theme.textColor
                                width: 8
                                height: 12
                            }
                        }
                    }
                }
            }
        }
    }
}
