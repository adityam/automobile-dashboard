import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.private.automobiledash 1.0 as AutoDash
import Mycroft 1.0 as Mycroft

ColumnLayout {
    id: messageRoot
    anchors.fill: parent

    ListModel {
        id: mailTopModelExample
        ListElement {
            senderIcon: "https://images-na.ssl-images-amazon.com/images/I/31AWFCkio3L._SX425_.jpg"
            sender: "Kim Apple"
            subject: "Fw: New Tyres Available"
            datetime: "Tue at 13.41"
        }
        ListElement {
            senderIcon: "https://images-na.ssl-images-amazon.com/images/I/41IK02LISNL._SX425_.jpg"
            sender: "Jes Mango"
            subject: "Car Service Schedule: CSB00N1"
            datetime: "Wed at 16.21"
        }
    }

    ListModel {
        id: kimConversationModel
        ListElement {
            senderIcon: "https://images-na.ssl-images-amazon.com/images/I/31AWFCkio3L._SX425_.jpg"
            from: "Kim Apple"
            inbound: true
            message: "Powered by artificial intelligence, the Eagle 360 Urban tyre is covered in a 'bionic skin' of sensors made from super-elastic polymer, which enables it to sense driving conditions, including terrain and weather, before feeding it back to the AI system to adapt itself accordingly."
            datetime: "Tue at 13.41"
        }
        ListElement {
            senderIcon: "https://images-na.ssl-images-amazon.com/images/I/31AWFCkio3L._SX425_.jpg"
            from: "You"
            inbound: false
            message: "What is the cost of each tyre?"
            datetime: "Tue at 13.45"
        }
    }

    Controls.Frame {
        Layout.fillWidth: true
        Layout.preferredHeight: parent.height / 2
        Layout.margins: Kirigami.Units.largeSpacing
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
            border.color: Kirigami.Theme.disabledTextColor
            radius: 2
        }
        ListView {
            model: kimConversationModel
            anchors.fill: parent
            clip: true
            spacing: Kirigami.Units.gridUnit * 3
            delegate: Item {
                id: rootConvo

                width: parent.width - Kirigami.Units.largeSpacing
                height: delegate.height

                ColumnLayout {
                    anchors.fill: parent

                    RowLayout {
                        Layout.fillWidth: true
                        spacing: Math.round(Kirigami.Units.gridUnit / 2)

                        Image {
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                            Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                            Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                            source: model.senderIcon
                        }

                        Kirigami.Heading {
                            Layout.fillWidth: true
                            Layout.alignment: Qt.AlignHCenter
                            height: paintedHeight
                            elide: Text.ElideRight
                            font.weight: Font.DemiBold
                            text: model.from
                            textFormat: Text.PlainText
                            level: 2
                        }
                    }

                    Rectangle {
                        id: delegate
                        radius: 4
                        color: Kirigami.Theme.backgroundColor
                        Layout.margins: Kirigami.Units.smallSpacing
                        Layout.preferredWidth: delcontent.contentWidth + Kirigami.Units.largeSpacing * 2
                        Layout.preferredHeight: delcontent.contentHeight + Kirigami.Units.largeSpacing * 2
                        Layout.alignment: model.inbound ? Qt.AlignLeft : Qt.AlignRight
                        layer.enabled: true
                        layer.effect: DropShadow {
                            horizontalOffset: 0
                            verticalOffset: Kirigami.Units.gridUnit/8
                            radius: Kirigami.Units.gridUnit/2
                            samples: 32
                            color: Qt.rgba(0, 0, 0, 0.5)
                        }

                        Controls.Label {
                            id: delcontent
                            width: rootConvo.width
                            padding: Kirigami.Units.largeSpacing
                            text: model.message
                            wrapMode: Text.WordWrap
                        }
                    }
                }
            }
        }
        RowLayout {
            anchors.bottom: parent.bottom
            height: Kirigami.Units.gridUnit * 2
            anchors.left: parent.left
            anchors.right: parent.right

            PlasmaComponents.TextField {
                Layout.fillHeight: true
                Layout.fillWidth: true
                placeholderText: "Message Reply"
                clearButtonShown: true
            }
            PlasmaComponents.Button {
                Layout.fillHeight: true
                Layout.preferredWidth: Kirigami.Units.gridUnit * 4
                text: "Reply"
            }
        }
    }
    Controls.Frame {
        Layout.fillWidth: true
        Layout.preferredHeight: parent.height / 2
        Layout.margins: Kirigami.Units.largeSpacing
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
            border.color: Kirigami.Theme.disabledTextColor
            radius: 2
        }
        ListView {
            model: mailTopModelExample
            anchors.fill: parent
            clip: true
            delegate: Kirigami.AbstractListItem {
                contentItem: Item {
                    implicitWidth: delegateLayout.implicitWidth;
                    implicitHeight: delegateLayout.implicitHeight;

                    ColumnLayout {
                        id: delegateLayout
                        anchors {
                            left: parent.left;
                            top: parent.top;
                            right: parent.right;
                        }

                        RowLayout {
                            Layout.fillWidth: true
                            spacing: Math.round(Kirigami.Units.gridUnit / 2)

                            Image {
                                Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                                Layout.preferredHeight: Kirigami.Units.iconSizes.medium
                                Layout.preferredWidth: Kirigami.Units.iconSizes.medium
                                source: model.senderIcon
                            }


                            Kirigami.Heading {
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHCenter
                                height: paintedHeight
                                elide: Text.ElideRight
                                font.weight: Font.DemiBold
                                text: model.sender
                                textFormat: Text.PlainText
                                level: 2
                            }

                            Controls.Label {
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHCenter
                                height: paintedHeight
                                elide: Text.ElideRight
                                font.weight: Font.Thin
                                text: model.subject
                            }

                            Controls.Label {
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignHCenter | Qt.AlignRight
                                height: paintedHeight
                                elide: Text.ElideRight
                                font.weight: Font.Thin
                                text: model.datetime
                            }
                        }
                    }
                }
            }
        }
    }
}
