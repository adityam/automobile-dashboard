import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import Mycroft 1.0 as Mycroft

ColumnLayout {
    anchors.fill: parent
    property var checkedRadioType
    property var frequencyType
    property bool radStop: false
    
    Controls.Switch {
        id: radioOnOffSwitch
        Layout.alignment: Qt.AlignRight
        Layout.rightMargin: Kirigami.Units.largeSpacing
        Layout.preferredWidth: Kirigami.Units.gridUnit * 7
        Layout.preferredHeight: Kirigami.Units.iconSizes.large
        text: "Radio"
        
        indicator: Rectangle {
        implicitWidth: Kirigami.Units.gridUnit * 4
        implicitHeight: parent.height / 2
        x: radioOnOffSwitch.leftPadding
        y: parent.height / 2 - height / 2
        radius: 13
        color: radioOnOffSwitch.checked ? Kirigami.Theme.backgroundColor : Kirigami.Theme.linkColor
        border.color: Kirigami.Theme.disabledTextColor

            Rectangle {
                x: radioOnOffSwitch.checked ? parent.width - width : 0
                width: Kirigami.Units.gridUnit * 2
                height: parent.height
                radius: 13
                color: radioOnOffSwitch.checked ? Kirigami.Theme.linkColor : Kirigami.Theme.backgroundColor
                border.color: Kirigami.Theme.disabledTextColor
                
                Text {
                    anchors.centerIn: parent
                    text: radioOnOffSwitch.checked ? "||" : "O"
                    color: radioOnOffSwitch.checked ? "Green" : "Red"
                }
            }
        }
    }
    
     MediaPlayer {
        id: playMusic
        source: "http://noisefm.ru:8000/live"
        property bool stopPlay: radStop
        onStopPlayChanged: {
            playMusic.stop()
        }
    }
    
    Connections {
        target: dashboard
        ignoreUnknownSignals: true
        
        onSigRadioPlayerChangeMode: {
            if(msgRadioPlayerChangeMode == "fm"){
                fmRadioType.checked = true
                checkedRadioType = "FM"
                frequencyType = "Mhz"
            } else if (msgRadioPlayerChangeMode == "am") {
                amRadioType.checked = true
                checkedRadioType = "AM"
                frequencyType = "Khz" 
            }
        }

        onSigRadioPlayerScanStations: {
            radioScanTimer.start()
        }
        onSigRadioPlayerSelectStation: {
            freqDial.value = "102.7"
            playMusic.source = "https://sampleswap.org/mp3/artist/27103/GAR_The-V-Day-160.mp3"
            playMusic.play()
        }
        onSigRadioPlayerTurnOff: {
            playMusic.stop()
            radioOnOffSwitch.checked = false
            radioOnOffSwitch.position = 0
        }
        onSigRadioPlayerTurnOn: {
            freqDial.value = "91.2"
            playMusic.source = "http://noisefm.ru:8000/live"
            playMusic.play()
            radioOnOffSwitch.checked = true
            radioOnOffSwitch.position = 1
        }
    }

    Component.onCompleted: {
        if(fmRadioType.checked){
            checkedRadioType = "FM"
            frequencyType = "Mhz"
        } else {
            checkedRadioType = "AM"
            frequencyType = "Khz"
        }
    }

    ListModel {
        id: radioFMStations
        ListElement{
            frequency: "91.2"
            stationId: "AFX FM"
        }
        ListElement{
            frequency: "94.5"
            stationId: "NPR Radio"
        }
        ListElement{
            frequency: "102.7"
            stationId: "Fixit News"
        }
        ListElement{
            frequency: "105.9"
            stationId: "National Radio"
        }
    }

    ListModel {
        id: radioAMStations
        ListElement{
            frequency: "565.5"
            stationId: "JAM AM"
        }
        ListElement{
            frequency: "690.2"
            stationId: "DD Radio"
        }
        ListElement{
            frequency: "702.7"
            stationId: "City News"
        }
        ListElement{
            frequency: "1205.9"
            stationId: "National Radio"
        }
    }

    PlasmaComponents.ButtonRow {
        id: radioTypeGroup
        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
        Layout.preferredHeight: Kirigami.Units.gridUnit * 2
        spacing: Kirigami.Units.smallSpacing

        PlasmaComponents.Button {
            id: fmRadioType
            checkable: true
            checked: true
            text: "FM"
            onClicked: {
                checkedRadioType = "FM"
                frequencyType = "Mhz"
            }
        }
        PlasmaComponents.Button {
            id: amRadioType
            checkable: true
            checked: false
            text: "AM"
            onClicked: {
                checkedRadioType = "AM"
                frequencyType = "Khz"
            }
        }
    }

    RowLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignHCenter
        
        Item {
            Layout.preferredWidth: freqDial.width
            Layout.preferredHeight: freqDial.height

            PlasmaComponents3.Dial {
                id: freqDial
                from: checkedRadioType == "FM" ? 88.1 : 540.1
                value: checkedRadioType == "FM" ? 90.1 : 565.5
                to: checkedRadioType == "FM" ? 108.1 : 1610.1
                stepSize: 0.1
                snapMode: Controls.Dial.SnapOnRelease
                width:  musicRoot.width / 3
                height:  musicRoot.height / 3
                anchors.centerIn: parent

                handle: Rectangle {
                    id: handleItem
                    x: freqDial.background.x + freqDial.background.width / 2 - width / 2
                    y: freqDial.background.y + freqDial.background.height / 2 - height / 2
                    width: Kirigami.Units.iconSizes.small
                    height: Kirigami.Units.iconSizes.small
                    color: freqDial.pressed ? Kirigami.Theme.disabledTextColor : Kirigami.Theme.linkColor
                    radius: 8
                    antialiasing: true
                    opacity: freqDial.enabled ? 1 : 0.3
                    transform: [
                        Translate {
                            y: -Math.min(freqDial.background.width, freqDial.background.height) * 0.37 + handleItem.height / 2
                        },
                        Rotation {
                            angle: freqDial.angle
                            origin.x: handleItem.width / 2
                            origin.y: handleItem.height / 2
                        }
                    ]
                }

                ColumnLayout{
                    anchors.centerIn: parent

                    Kirigami.Heading {
                        text: "Freq"
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                        font.italic: true
                        level: 2
                    }

                    Kirigami.Heading {
                        text: "<b>" + freqDial.value.toFixed(1) + "</b>" + frequencyType
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        font.letterSpacing: 2
                        level: 3
                    }
                }
            }
        }
        Item {
            Layout.preferredWidth: volumeDial.width
            Layout.preferredHeight: volumeDial.height

            PlasmaComponents3.Dial {
                id: volumeDial
                from: 0
                value: 42
                to: 100
                stepSize: 1
                width:  musicRoot.width / 3
                height:  musicRoot.height / 3
                anchors.centerIn: parent

                handle: Rectangle {
                    id: handleItemVol
                    x: volumeDial.background.x + volumeDial.background.width / 2 - width / 2
                    y: volumeDial.background.y + volumeDial.background.height / 2 - height / 2
                    width: Kirigami.Units.iconSizes.small
                    height: Kirigami.Units.iconSizes.small
                    color: volumeDial.pressed ? Kirigami.Theme.disabledTextColor : Kirigami.Theme.linkColor
                    radius: 8
                    antialiasing: true
                    opacity: volumeDial.enabled ? 1 : 0.3
                    transform: [
                        Translate {
                            y: -Math.min(volumeDial.background.width, volumeDial.background.height) * 0.37 + handleItemVol.height / 2
                        },
                        Rotation {
                            angle: volumeDial.angle
                            origin.x: handleItemVol.width / 2
                            origin.y: handleItemVol.height / 2
                        }
                    ]
                }

                ColumnLayout{
                    anchors.centerIn: parent

                    Kirigami.Heading {
                        text: "Vol"
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                        font.italic: true
                        level: 2
                    }

                    Kirigami.Heading {
                        text: "<b>" + volumeDial.value.toFixed(0) + "</b>" + "%"
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        font.letterSpacing: 2
                        level: 3
                    }
                }
            }
        }
    }

    Rectangle {
        id: radioLayoutBottomRect
        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
        border.color: Kirigami.Theme.disabledTextColor
        border.width: 0.5
        Layout.alignment: Qt.AlignBottom
        Layout.preferredHeight: radioScanTimer.running ? (radioScanBar.height + scanRadioChannelsButton.height + Kirigami.Units.largeSpacing) : parent.height / 2.5
        Layout.fillWidth: true

        ColumnLayout {
            anchors.fill: parent

            PlasmaComponents.Button {
                id: scanRadioChannelsButton
                Layout.fillWidth: true
                Layout.preferredHeight: Kirigami.Units.gridUnit * 2
                Layout.alignment: Qt.AlignTop
                text: "Scan Radio Stations"
                iconSource: "refactor"
                onClicked: radioScanTimer.start()
            }

            Kirigami.Heading {
                id: availableStationLabel
                text: "Available Radio Stations"
                level: 2
                font.bold: true
                Layout.topMargin: Kirigami.Units.largeSpacing
                Layout.leftMargin: Kirigami.Units.largeSpacing
                Layout.bottomMargin: Kirigami.Units.largeSpacing
            }

            Timer {
                id: radioScanTimer
                interval: 6000
                running: false
                repeat: false
                onRunningChanged: {
                    if(running) {
                        radioScanBar.visible = true
                        radioScanBar.enabled = true
                        availableStationLabel.visible =false
                        stationListView.visible = false
                        stationListView.enabled = false
                    }
                    if(!running) {
                        radioScanBar.visible = false
                        radioScanBar.enabled = false
                        availableStationLabel.visible = true
                        stationListView.visible = true
                        stationListView.enabled = true
                    }
                }
            }

            Controls.ProgressBar {
                id: radioScanBar
                Layout.fillWidth: true
                Layout.leftMargin: Kirigami.Units.largeSpacing
                Layout.rightMargin: Kirigami.Units.largeSpacing
                Layout.bottomMargin: Kirigami.Units.largeSpacing
                Layout.preferredHeight: Kirigami.Units.gridUnit * 1.25
                Layout.alignment: Qt.AlignVCenter
                indeterminate: true
                visible: false
                enabled: false

                background: Rectangle {
                    implicitWidth: parent.width
                    implicitHeight: parent.height
                    color: Kirigami.Theme.linkColor
                    radius: 5
                    border.color: Kirigami.Theme.disabledTextColor
                    border.width: 1
                }
                contentItem: Item {
                    implicitWidth: parent.width / 2
                    implicitHeight: parent.height

                    RowLayout {
                        id: indicator
                        height: parent.height
                        width: Kirigami.Units.gridUnit * 3 + Kirigami.Units.smallSpacing
                        spacing: Kirigami.Units.smallSpacing
                        Rectangle {
                            Layout.preferredWidth: Kirigami.Units.gridUnit * 1
                            Layout.fillHeight: true
                            border.color: Kirigami.Theme.disabledTextColor
                            border.width: 1
                            radius: 6
                            color: Kirigami.Theme.backgroundColor
                        }
                        Rectangle {
                            Layout.preferredWidth: Kirigami.Units.gridUnit * 1
                            Layout.fillHeight: true
                            border.color: Kirigami.Theme.disabledTextColor
                            border.width: 1
                            radius: 6
                            color: Kirigami.Theme.backgroundColor
                        }
                        Rectangle {
                            Layout.preferredWidth: Kirigami.Units.gridUnit * 1
                            Layout.fillHeight: true
                            border.color: Kirigami.Theme.disabledTextColor
                            border.width: 1
                            radius: 6
                            color: Kirigami.Theme.backgroundColor
                        }
                    }

                    SequentialAnimation {
                        id: anim
                        loops: Animation.Infinite
                        running: radioScanBar.indeterminate && radioScanBar.visible
                        PropertyAnimation {
                            target: indicator
                            property: "x"
                            duration: 2000
                            easing.type: Easing.Linear
                            to: radioScanBar.width - indicator.width
                            onToChanged: {
                                if (anim.running) {
                                    anim.restart();
                                }
                            }
                        }
                        PropertyAnimation {
                            target: indicator
                            property: "x"
                            easing.type: Easing.Linear
                            duration: 2000
                            to: 0
                        }
                    }
                }
            }

            ListView {
                id: stationListView
                model: checkedRadioType == "FM" ? radioFMStations : radioAMStations
                clip: true
                visible: true
                enabled: true
                Layout.fillHeight: true
                Layout.fillWidth: true
                delegate: Kirigami.AbstractListItem {
                    highlighted: freqDial.value.toFixed(1) == frequency

                    contentItem: Item {
                        implicitWidth: delegateLayout.implicitWidth;
                        implicitHeight: delegateLayout.implicitHeight;

                        ColumnLayout {
                            id: delegateLayout
                            anchors {
                                left: parent.left;
                                top: parent.top;
                                right: parent.right;
                            }

                            RowLayout {
                                Layout.fillWidth: true
                                spacing: Math.round(units.gridUnit / 2)

                                Kirigami.Icon {
                                    id: radioSvgIcon
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                                    Layout.preferredHeight: units.iconSizes.medium
                                    Layout.preferredWidth: units.iconSizes.medium
                                    color: Kirigami.Theme.textColor
                                    source: "games-difficult"
                                }

                                Kirigami.Heading {
                                    Layout.alignment: Qt.AlignVCenter | Qt.AlignLeft
                                    level: 2
                                    elide: Text.ElideRight
                                    textFormat: Text.PlainText
                                    id: textBxStationID
                                    text: stationId
                                }

                                Kirigami.Heading {
                                    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                                    level: 2
                                    elide: Text.ElideRight
                                    textFormat: Text.PlainText
                                    id: textBxStationFreq
                                    text: frequency + frequencyType
                                }
                            }
                        }
                    }
                    onClicked: {
                        freqDial.value = frequency
                    }
                }
            }
        }
    }
}
