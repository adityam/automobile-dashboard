/*
    Copyright 2019 Aditya Mehra <aix.m@outlook.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.private.automobiledash 1.0 as AutoDash
import AudioProbe 1.0
import Mycroft 1.0 as Mycroft

Item {
    id: root

    implicitWidth: Kirigami.Units.gridUnit * 60
    implicitHeight: Kirigami.Units.gridUnit * 40

    Layout.minimumWidth: Kirigami.Units.gridUnit * 10
    Layout.minimumHeight: Kirigami.Units.gridUnit * 15
    property var spectrum: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    property var soundModelLength: audProb.spectrum.length
    property var playbackState: audProb.playbackState
    property var trackLength
    property var trackPosition
    property var albumArt
    property var trackTitle
    property var msgNotif
    
    function playFromURL(getURL){
            audProb.playURL(getURL)
            console.log(audProb.length())
    }
        
    function pausePlayer(){
            audProb.playerPause()
    }
        
    function stopPlayer(){
            audProb.playerStop()
    }
    
    function continuePlayer(){
            audProb.playerContinue()
    }
    
    function seekPlayer(val){
        audProb.playerSeek(val)
    }
        
    Connections {
        target: audProb
        onDurationChanged: {
            trackLength = dur
        }
        onPositionChanged: {
            trackPosition = pos
        }
    }
    
    Connections {
        target: dashboard
        ignoreUnknownSignals: true
        onSendShowAutoDash: {
            leftTabBarLoader.currentIndex = 0
        }
        onSendShowNavigation: {
            leftTabBarLoader.currentIndex = 1
        }
        onSendShowMusic: {
            leftTabBarLoader.currentIndex = 2
        }
        onSendShowMessages: {
            leftTabBarLoader.currentIndex = 3
        }
        onSendActivateVoiceCmd: {
            console.log("Activate Voice Command Via Button")
        }
        onSigShowNotif: {
           msgNotif = msgShowNotif 
           pnotif.open()
           pcloser.running = true
        }
    }
    
    Component.onCompleted: {
        Mycroft.MycroftController.start();
        audProb.setupSource();
    }
    
    Timer {
        id: sampler
        running: true
        interval: 100
        repeat: true
        onTriggered: {
            spectrum = audProb.spectrum
        }
    }
    
    Timer {
        id: pcloser
        running: false
        interval: 8000
        onTriggered: {
            pnotif.close();
        }
    }
        
    AudioProbe {
        id: audProb
    }
        
    Frame {
        anchors.fill: parent
        anchors.margins: 5

        Item {
            anchors.right: parent.right
            width: Kirigami.Units.gridUnit * 18
            height: Kirigami.Units.gridUnit * 4
            
            Popup {
                id: pnotif
                width: Kirigami.Units.gridUnit * 16
                height: Kirigami.Units.gridUnit * 4
                topInset: -0
                leftInset: -0
                rightInset: -0
                bottomInset: -0
                
                Rectangle {
                    anchors.fill: parent
                    color: Kirigami.Theme.backgroundColor
                    Kirigami.InlineMessage {
                        anchors.fill: parent
                        type: Kirigami.MessageType.Information
                        text: msgNotif
                        visible: pnotif.opened
                    }
                }
            }
            
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    pnotif.close()
                }
            }
        }
        
        background: Rectangle {
            color: Kirigami.Theme.backgroundColor
            border.color: Kirigami.Theme.disabledTextColor
            radius: 2
        }

        ButtonGroup {
            buttons: columnLayout.children
        }

        RowLayout {
            id: mainRowLayout
            anchors.fill: parent
            anchors.margins: 10
            spacing: 36

            GridLayout {
                id: columnLayout
                Layout.maximumWidth: root.width / 8
                Layout.fillHeight: true
                columns: 2
                //spacing: Kirigami.Units.largeSpacing

                Button {
                    id: dashboardFeatureButton
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width > Kirigami.Units.iconSizes.small ? parent.width : Kirigami.Units.iconSizes.medium
                    //text: "Dashboard"
                    onClicked: {
                        //leftTabBarLoader.push("DashboardSheet.qml")
                        leftTabBarLoader.currentIndex = 0
                    }
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: dashboardFeatureButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                    }
                    contentItem: RowLayout {
                        spacing: -Kirigami.Units.gridUnit * 1
                        Kirigami.Icon {
                            id: dashboardFeatureButtonIcon
                            source: "office-chart-polar-stacked"
                            width: Kirigami.Units.iconSizes.medium
                            height: Kirigami.Units.iconSizes.medium
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            Layout.leftMargin: Kirigami.Units.largeSpacing * 2
                        }
                        Label {
                            text: "Dashboard"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }
                }

                Button {
                    id: navigationFeatureButton
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width > Kirigami.Units.iconSizes.small ? parent.width : Kirigami.Units.iconSizes.medium
                    onClicked: {
                        leftTabBarLoader.currentIndex = 1
                    }
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: navigationFeatureButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                    }
                    contentItem: RowLayout {
                        spacing: -Kirigami.Units.gridUnit * 1
                        Kirigami.Icon {
                            id: navigationFeatureButtonIcon
                            source: "arrow"
                            width: Kirigami.Units.iconSizes.medium
                            height: Kirigami.Units.iconSizes.medium
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            Layout.leftMargin: Kirigami.Units.largeSpacing * 2
                        }
                        Label {
                            text: "Navigation"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }
                }

                Button {
                    id: musicFeatureButton
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width > Kirigami.Units.iconSizes.small ? parent.width : Kirigami.Units.iconSizes.medium
                    onClicked: {
                            //leftTabBarLoader.source = "MusicSheet.qml"
                            leftTabBarLoader.currentIndex = 2
                    }
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: musicFeatureButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                    }
                    contentItem: RowLayout {
                        spacing: -Kirigami.Units.gridUnit * 3
                        Kirigami.Icon {
                            id: musicFeatureButtonIcon
                            source: "media-write-cd"
                            width: Kirigami.Units.iconSizes.medium
                            height: Kirigami.Units.iconSizes.medium
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            Layout.leftMargin: Kirigami.Units.largeSpacing * 2
                        }
                        Label {
                            text: "Music"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }
                }

                Button {
                    id: messageFeatureButton
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width > Kirigami.Units.iconSizes.small ? parent.width : Kirigami.Units.iconSizes.medium
                    onClicked: {
                        leftTabBarLoader.currentIndex = 3
                    }
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: messageFeatureButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                    }
                    contentItem: RowLayout {
                        spacing: -Kirigami.Units.gridUnit * 1
                        Kirigami.Icon {
                            id: messageFeatureButtonIcon
                            source: "postalcode"
                            width: Kirigami.Units.iconSizes.medium
                            height: Kirigami.Units.iconSizes.medium
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            Layout.leftMargin: Kirigami.Units.largeSpacing * 2
                        }
                        Label {
                            text: "Message"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }
                }

                Button {
                    id: voiceCommandFeatureButton
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width > Kirigami.Units.iconSizes.small ? parent.width : Kirigami.Units.iconSizes.medium
                    onClicked: {
                        console.log("Voice")
                    }
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: voiceCommandFeatureButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                    }
                    contentItem: RowLayout {
                        spacing: -Kirigami.Units.gridUnit * 1
                        Kirigami.Icon {
                            id: voiceCommandFeatureButtonIcon
                            source: "mic-on"
                            width: Kirigami.Units.iconSizes.medium
                            height: Kirigami.Units.iconSizes.medium
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            Layout.leftMargin: Kirigami.Units.largeSpacing * 2
                        }
                        Label {
                            text: "Command"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }
                }

                Button {
                    id: settingsFeatureButton
                    Layout.fillHeight: true
                    Layout.preferredWidth: parent.width > Kirigami.Units.iconSizes.small ? parent.width : Kirigami.Units.iconSizes.medium
                    onClicked: {
                        leftTabBarLoader.currentIndex = 4
                    }
                    background: Rectangle {
                        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
                        border.color: settingsFeatureButton.down ? Kirigami.Theme.textColor : Kirigami.Theme.disabledTextColor
                        border.width: 1
                    }
                    contentItem: RowLayout {
                        spacing: -Kirigami.Units.gridUnit * 1
                        Kirigami.Icon {
                            id: settingsFeatureButtonIcon
                            source: "configure"
                            width: Kirigami.Units.iconSizes.medium
                            height: Kirigami.Units.iconSizes.medium
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                            Layout.leftMargin: Kirigami.Units.largeSpacing * 2
                        }
                        Label {
                            text: "Settings"
                            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
                        }
                    }
                }
            }

            StackLayout {
                id: leftTabBarLoader
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                Layout.leftMargin: root.width / 8
                Layout.fillHeight: true
                currentIndex: 0
                clip: true
                
                Item {
                    DashboardSheet{}
                }                
                Item {
                    NavigationSheet{}
                }
                Item {
                    MusicSheet{}
                }
                Item {
                    MessageSheet{}
                }
                Item {
                    SettingsSheet{}
                }
            }
        }
    }
}
