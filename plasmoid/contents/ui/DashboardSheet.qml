import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.private.automobiledash 1.0 as AutoDash
import AudioProbe 1.0
import org.kde.plasma.components 2.0 as PlasmaComponents

Item {
    anchors.fill: parent
    property int currentTemp: 16
    property int currentSpeed: 30

    function setTemp(currentTemp){
        var setTempValue = Math.min(Math.max(currentTemp, 16), 24);
        return setTempValue
    }
    
    Rectangle {
        anchors.bottom: parent.bottom
        width: parent.width
        height: Kirigami.Units.iconSizes.medium + Kirigami.Units.largeSpacing
        color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
        border.color: Kirigami.Theme.disabledTextColor
        border.width: 0.5
        visible: root.playbackState === MediaPlayer.PlayingState || root.playbackState === MediaPlayer.PausedState ? 1 : 0 
        enabled: root.playbackState === MediaPlayer.PlayingState || root.playbackState === MediaPlayer.PausedState ? 1 : 0
        MusicPlayerMini{}
    }
    
    ColumnLayout {
        anchors.fill: parent

        PlasmaComponents.TabBar {
            id: tabBar
            Layout.preferredWidth: parent.width / 2

            PlasmaComponents.TabButton {
                id: externalTab
                text: qsTr("External Dashboard")
            }

            PlasmaComponents.TabButton {
                id: internalTab
                text: qsTr("Internal Dashboard")
            }
        }

        ExternalDashboard {
            Layout.fillWidth: true
            visible: tabBar.currentTab == externalTab
        }

        InternalDashboard {
            Layout.fillWidth: true
            visible: tabBar.currentTab == internalTab
        }
    }
}
