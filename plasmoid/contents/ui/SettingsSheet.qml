import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.private.automobiledash 1.0 as AutoDash
import Mycroft 1.0 as Mycroft

ColumnLayout {
    id: settingsRoot
    anchors.fill: parent

    Kirigami.FormLayout {
    anchors.left: parent.left
    anchors.right: parent.right
                    
        PlasmaComponents.TextField {
            id: websocketAddress
            Layout.fillWidth: true
            Kirigami.FormData.label: i18n("Websocket Address:")       
            Component.onCompleted: {
                websocketAddress.text = Mycroft.GlobalSettings.webSocketAddress
            }
        }
        
        CheckBox {
            id: notificationSwitch
            Kirigami.FormData.label: i18n ("Additional Settings:")
            text: i18n("Enable Notifications")
            checked: true
        }
        
        CheckBox {
            id: enableRemoteTTS
            text: i18n("Enable Remote TTS")
            checked: Mycroft.GlobalSettings.usesRemoteTTS
            onCheckedChanged: Mycroft.GlobalSettings.usesRemoteTTS = checked
        }
        
        CheckBox {
            id: enableRemoteSTT
            text: i18n("Enable Remote STT")
            checked: false
        }
    }
}
