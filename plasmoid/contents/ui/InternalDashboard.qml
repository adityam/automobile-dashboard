﻿import QtQuick 2.9
import QtQuick.Controls 2.4 as Controls
import QtQuick.Layouts 1.3
import QtMultimedia 5.9
import org.kde.kirigami 2.8 as Kirigami
import org.kde.plasma.components 2.0 as PlasmaComponents
import Mycroft 1.0 as Mycroft

RowLayout{
    Layout.fillWidth: true
    spacing: 0
    property int leftSeatCurrentAngle: 110
    property int rightSeatCurrentAngle: 110
    
    Connections {
        target: dashboard
        ignoreUnknownSignals: true
        
        onSigInternalDashAdjLeftSeat: {
            setLeftSeatAdjustment(msgInternalDashAdjLeftSeat)
            if(msgInternalDashAdjLeftSeatType == "increase"){
                if(leftSeatCurrentAngle < 130) {
                    leftSeatCurrentAngle += 10
                }
                setLeftSeatAdjustment(leftSeatCurrentAngle)
                Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "Left seat pitch has been increased"})
            } else if (msgInternalDashAdjLeftSeatType == "decrease"){
                if(leftSeatCurrentAngle > 80) {
                    leftSeatCurrentAngle -= 10
                }
                setLeftSeatAdjustment(leftSeatCurrentAngle)
                Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "Left seat pitch has decreased"})
            } else if (msgInternalDashAdjLeftSeatType == "manual"){
                if(msgInternalDashAdjLeftSeat < 130 || msgInternalDashAdjLeftSeat > 80){
                    leftSeatCurrentAngle = Math.round(msgInternalDashAdjLeftSeat / 10) * 10 
                }
            }
        }
        
        onSigInternalDashAdjRightSeat: {
            setRightSeatAdjustment(msgInternalDashAdjRightSeat)
            if(msgInternalDashAdjRightSeatType == "increase"){
                if(rightSeatCurrentAngle < 130) {
                    rightSeatCurrentAngle += 10
                }
                setRightSeatAdjustment(rightSeatCurrentAngle)
                Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "Right seat pitch has been increased"})
            } else if (msgInternalDashAdjRightSeatType == "decrease"){
                if(rightSeatCurrentAngle > 80) {
                    rightSeatCurrentAngle -= 10
                }
                setRightSeatAdjustment(rightSeatCurrentAngle)
                Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "Right seat pitch has been decreased"})
            } else if (msgInternalDashAdjRightSeatType == "manual"){
                if(msgInternalDashAdjRightSeat < 130 || msgInternalDashAdjRightSeat > 80){
                    rightSeatCurrentAngle = Math.round(msgInternalDashAdjRightSeat / 10) * 10 
                }
            }
        }
        
        onSigInternalDashAdjAcTemp: {
            setTemp(msgInternalDashAdjAcTemp);
            if (msgInternalDashAdjAcTempType == "increase") {
                if(currentTemp < 24){
                    currentTemp += 1
                }
                setTemp(currentTemp)
                Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The air temp has increased"})
            } else if(msgInternalDashAdjAcTempType == "decrease"){
                if(currentTemp > 16) {
                    currentTemp -= 1
                }
                setTemp(currentTemp)
                Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "The air temp has decreased"})
            } else if(msgInternalDashAdjAcTempType == "manual"){
                if(msgInternalDashAdjAcTemp < 24 || msgInternalDashAdjAcTemp > 16){
                    currentTemp = msgInternalDashAdjAcTemp
                }
            }
        }
        
        onSigInternalDashAdjSteerHeight: {
            console.log(msgInternalDashAdjSteerHeight);
            if(msgInternalDashAdjSteerHeightType == "increase"){
                wheelbox.value += 1
              Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "Steering wheel height adjusted"})
            } else if(msgInternalDashAdjSteerHeightType == "decrease"){
                wheelbox.value -= 1
              Mycroft.MycroftController.sendRequest("automobile.skill.speak", {"speak": "Steering wheel height adjusted"})
            } else if(msgInternalDashAdjSteerHeightType == "manual"){
                wheelbox.value = msgInternalDashAdjSteerHeight
            }
        }
        
        onSigInternalDashLights: {
            if(msgInternalDashLights){
                lightbuttonlabel.color = "Green"
                lightbuttonstate.text = "ON"
            } else {
                lightbuttonlabel.color = "Red"
                lightbuttonstate.text = "OFF"
            }
        }
    }

    function setLeftSeatAdjustment(leftSeatCurrentAngle){
        var setLeftAdjValue = Math.min(Math.max(leftSeatCurrentAngle, 80), 130);
        return setLeftAdjValue
    }

    function setRightSeatAdjustment(rightSeatCurrentAngle){
        var setRightAdjValue = Math.min(Math.max(rightSeatCurrentAngle, 80), 130);
        return setRightAdjValue
    }

    RowLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true

        Rectangle {
            id: rectSeatLeftLabel
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 0.5
            Layout.alignment: Qt.AlignAbsolute
            Layout.preferredWidth: Kirigami.Units.gridUnit * 8
            Layout.preferredHeight: Kirigami.Units.gridUnit * 9
            Kirigami.Heading {
                id: leftSeatBoxHeading
                level: 3
                text: "Front Left Seat"
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Kirigami.Separator {
                id: leftSeatBoxSept
                width: parent.width
                height: 1
                anchors.top: leftSeatBoxHeading.bottom
                anchors.topMargin: Kirigami.Units.smallSpacing
            }
            ColumnLayout{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: Kirigami.Units.largeSpacing

                Kirigami.Icon {
                    Layout.preferredWidth: Kirigami.Units.iconSizes.large
                    Layout.preferredHeight: width
                    source: "arrow-up"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(leftSeatCurrentAngle < 130) {
                                leftSeatCurrentAngle += 10
                            }
                            setLeftSeatAdjustment(leftSeatCurrentAngle)
                        }
                    }
                }

                Kirigami.Heading {
                    id: leftSeatBoxStatus
                    level: 2
                    text: setLeftSeatAdjustment(leftSeatCurrentAngle) + "°"
                    color: Kirigami.Theme.linkColor
                    Layout.alignment: Qt.AlignHCenter
                    Layout.topMargin: -Kirigami.Units.largeSpacing
                    Layout.bottomMargin: -Kirigami.Units.largeSpacing
                }

                Kirigami.Icon {
                    Layout.preferredWidth: Kirigami.Units.iconSizes.large
                    Layout.preferredHeight: width
                    source: "arrow-down"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(leftSeatCurrentAngle > 80) {
                                leftSeatCurrentAngle -= 10
                            }
                            setLeftSeatAdjustment(leftSeatCurrentAngle)
                        }
                    }
                }
            }
        }
        Image {
            Layout.fillHeight: true
            Layout.preferredWidth: Kirigami.Units.gridUnit * 14
            source: "../images/CarInternal.png"
            fillMode: Image.PreserveAspectFit
        }
        Rectangle {
            id: rectDoorRightLabel
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 0.5
            Layout.alignment: Qt.AlignAbsolute
            Layout.preferredWidth: Kirigami.Units.gridUnit * 8
            Layout.preferredHeight: Kirigami.Units.gridUnit * 9
            Kirigami.Heading {
                id: rightDoorBoxHeading
                level: 3
                text: "Front Right Seat"
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Kirigami.Separator {
                id: rightDoorBoxSept
                width: parent.width
                height: 1
                anchors.top: rightDoorBoxHeading.bottom
                anchors.topMargin: Kirigami.Units.smallSpacing
            }
            ColumnLayout{
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.bottomMargin: Kirigami.Units.largeSpacing

                Kirigami.Icon {
                    Layout.preferredWidth: Kirigami.Units.iconSizes.large
                    Layout.preferredHeight: width
                    source: "arrow-up"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(rightSeatCurrentAngle < 130) {
                                rightSeatCurrentAngle += 10
                            }
                            setRightSeatAdjustment(rightSeatCurrentAngle)
                        }
                    }
                }

                Kirigami.Heading {
                    id: rightSeatBoxStatus
                    level: 2
                    text: setRightSeatAdjustment(rightSeatCurrentAngle) + "°"
                    color: Kirigami.Theme.linkColor
                    Layout.alignment: Qt.AlignHCenter
                    Layout.topMargin: -Kirigami.Units.largeSpacing
                    Layout.bottomMargin: -Kirigami.Units.largeSpacing
                }

                Kirigami.Icon {
                    Layout.preferredWidth: Kirigami.Units.iconSizes.large
                    Layout.preferredHeight: width
                    source: "arrow-down"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(rightSeatCurrentAngle > 80) {
                                rightSeatCurrentAngle -= 10
                            }
                            setRightSeatAdjustment(rightSeatCurrentAngle)
                        }
                    }
                }
            }
        }
    }

    ColumnLayout {
        Layout.fillHeight: true
        Layout.preferredWidth: Kirigami.Units.gridUnit * 11
        spacing: Kirigami.Units.largeSpacing * 2
        Layout.leftMargin: Kirigami.Units.gridUnit * 3
        Layout.alignment: Qt.AlignRight

        Rectangle {
            id: tempRect
            Layout.preferredWidth: Kirigami.Units.gridUnit * 10
            Layout.preferredHeight: Kirigami.Units.gridUnit * 8
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 1
            radius: 2

            ColumnLayout {
                anchors.fill: parent
                spacing: Kirigami.Units.smallSpacing

                Kirigami.Heading {
                    id: tempLabel
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredHeight: Kirigami.Units.gridUnit * 0.75
                    text: "Temperature"
                    color: Kirigami.Theme.linkColor
                    level: 1
                }

                Rectangle {
                    color: Kirigami.Theme.disabledTextColor
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: 1
                }

                RowLayout {
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                    Kirigami.Icon {
                        Layout.preferredWidth: Kirigami.Units.iconSizes.large
                        Layout.preferredHeight: width
                        source: "arrow-left"

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if(currentTemp > 16) {
                                    currentTemp -= 1
                                }
                                setTemp(currentTemp)
                            }
                        }
                    }
                    Kirigami.Heading {
                        level: 1
                        Layout.alignment:  Qt.AlignVCenter
                        text: setTemp(currentTemp) + "°"
                    }
                    Kirigami.Icon {
                        Layout.preferredWidth: Kirigami.Units.iconSizes.large
                        Layout.preferredHeight: width
                        source: "arrow-right"

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                if(currentTemp < 24){
                                    currentTemp += 1
                                }
                                setTemp(currentTemp)
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: steeringRect
            Layout.preferredWidth: Kirigami.Units.gridUnit * 10
            Layout.preferredHeight: Kirigami.Units.gridUnit * 8
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 1
            radius: 2

            ColumnLayout {
                anchors.fill: parent

                Kirigami.Heading {
                    id: steeringAdjLevelLabel
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredHeight: Kirigami.Units.gridUnit * 0.75
                    text: "Steer Height"
                    color: Kirigami.Theme.linkColor
                    level: 1
                }

                Rectangle {
                    color: Kirigami.Theme.disabledTextColor
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: 1
                }

                Controls.SpinBox {
                    id: wheelbox
                    from: -5
                    to: 5
                    value: 0
                    stepSize: 1
                    Layout.alignment: Qt.AlignHCenter
                    Kirigami.Theme.inherit: false

                    textFromValue: function(value) {
                           return Number(value / 1) + "°"
                    }
                }
            }
        }
        Rectangle {
            id: lightBoxExternal
            Layout.preferredWidth: Kirigami.Units.gridUnit * 10
            Layout.preferredHeight: Kirigami.Units.gridUnit * 8
            color: Qt.darker(Kirigami.Theme.backgroundColor, 1.2)
            border.color: Kirigami.Theme.disabledTextColor
            border.width: 1
            radius: 2

            ColumnLayout {
                anchors.fill: parent

                Kirigami.Heading {
                    id: lightBoxLabel
                    Layout.alignment: Qt.AlignHCenter
                    Layout.preferredHeight: Kirigami.Units.gridUnit * 0.5
                    text: "Lights"
                    color: Kirigami.Theme.linkColor
                    level: 1
                }

                Rectangle {
                    color: Kirigami.Theme.disabledTextColor
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: 1
                }

                RowLayout {
                    Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter

                    Rectangle {
                        id: lightbuttonlabel
                        Layout.preferredWidth: Kirigami.Units.iconSizes.small
                        Layout.preferredHeight: Kirigami.Units.iconSizes.small
                        radius: 100
                        color: "Green"
                    }

                    Kirigami.Heading {
                        id: lightbuttonstate
                        level: 2
                        Layout.alignment:  Qt.AlignVCenter
                        text: "ON"
                    }
                }
            }
        }
    }
}
